/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.abstracts;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import static java.lang.String.format;
import java.util.Arrays;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
public class AbstractDatabaseConnection {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AbstractDatabaseConnection.class);
    private static final AbstractDatabaseConnection INSTANCE = new AbstractDatabaseConnection();

    private MongoClient mongo = null;
    private Datastore dataStore = null;
    private Morphia morphia = null;
    private final String dbName = "test";

    private AbstractDatabaseConnection() {
    }

    public MongoClient getMongo() throws RuntimeException {
        if (mongo == null) {
            LOG.debug("Starting Mongo");
            MongoClientOptions.Builder options = MongoClientOptions.builder()
                    .connectionsPerHost(4)
                    .maxConnectionIdleTime((60 * 1_000))
                    .maxConnectionLifeTime((120 * 1_000));

            //MongoClientURI uri = new MongoClientURI("mongodb://testMaymus:maymusTest2017@ds155473.mlab.com:55473/maymus_test");
            try {
                MongoCredential credential = MongoCredential.createCredential("maymusTest", dbName, "bachelor".toCharArray());

                ServerAddress server = new ServerAddress("localhost", 27017);
                LOG.info("About to connect to MongoDB @ " + server.toString());
                mongo = new MongoClient(server, Arrays.asList(credential));
            } catch (MongoException ex) {
                LOG.error("An error occoured when connecting to MongoDB", ex);
            } catch (Exception ex) {
                LOG.error("An error occoured when connecting to MongoDB", ex);
            }
        }

        return mongo;
    }

    public Morphia getMorphia() {
        if (morphia == null) {
            LOG.debug("Starting Morphia");
            morphia = new Morphia();
            morphia.mapPackage("de.maymus.models");
            morphia.getMapper().getOptions().setStoreEmpties(true);

        }

        return morphia;
    }

    public Datastore getDatastore() {
        if (dataStore == null) {
            LOG.debug("Starting DataStore on DB: " + dbName);
            dataStore = getMorphia().createDatastore(getMongo(), dbName);
            dataStore.ensureIndexes();
        }

        return dataStore;
    }

    public void init() {
        LOG.debug("Bootstraping");
        getMongo();
        getMorphia();
        getDatastore();
    }

    public void close() {
        LOG.info("Closing MongoDB connection");
        if (mongo != null) {
            try {
                mongo.close();
                LOG.debug("Nulling the connection dependency objects");
                mongo = null;
                morphia = null;
                dataStore = null;
            } catch (Exception e) {
                LOG.error(format("An error occurred when closing the MongoDB connection\n%s", e.getMessage()));
            }
        } else {
            LOG.warn("mongo object was null, wouldn't close connection");
        }
    }

    public static AbstractDatabaseConnection getInstance() {
        return INSTANCE;
    }

//     public AbstractDatabaseConnection() {
//        LOG.info("====== Starting MongoCient TEST======");
//        if (INSTANCE == null) {
//            Properties properties = new Properties();
//
//            try {
//                InputStream in = new FileInputStream("src/test/resources/test.properties");
//                properties.load(in);
//                LOG.info("== .Props File: " + properties.toString() + ",   " + in.toString());
//                database = properties.getProperty("db.maymus.name");
//                username = properties.getProperty("db.maymus.user");
//                password = properties.getProperty("db.maymus.paswd");
//                connection = properties.getProperty("db.maymus.url");
//                auth = properties.getProperty("db.maymus.auth");
//                link = properties.getProperty("db.maymus.link");
//
//            } catch (Exception ex) {
//                LOG.error("Unknown Exception: " + ex);
//            }
//            String url = link + username + ":" + password + connection + database + auth;
//            MongoClientURI uri = new MongoClientURI(url);
//            MongoClient mongoClient = new MongoClient(uri);
//
//            //create a new morphia instance
//            this.morphia = new Morphia();
//            String databaseName = database;
//            this.datastore = morphia.createDatastore(mongoClient, databaseName);
//        }
//    }
}
