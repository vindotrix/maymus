/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import java.util.Properties;
import java.util.Random;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.junit.Test;

/**
 *
 * @author Kevin
 */
public class TestEmail {

    String user = "Vindotrix";
    String receiver = "acceber.89@gmail.com";
    String password = "ABasdlfk23890";

    @Test
    public void sendWelcomeMail() {
        sendMail(getWelcomeContent(user), receiver, "Welcome to Maymus!");
        //sendMail(genPasswortChangeNotify(user), receiver, "You've changed your password!");
        //sendMail(genNewPassword(user, password), receiver, "Your new password!");

    }

    private void sendMail(String content, String receiver, String subject) {

        final String mail_adress = "contact.maymus@gmail.com";
        final String mail_pw = "adminMaymus";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mail_adress, mail_pw);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail_adress));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(receiver));
            message.setSubject(subject);
            message.setContent(content, "text/html");
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

    private String getWelcomeContent(String username) {
        String message
                = "<h2 style=\"text-align: left;\"><span style=\"color: #003300;\"><strong>Welcome to Maymus!</strong></span></h2>\n"
                + "<p>&nbsp;</p>\n"
                + "<p><span style=\"color: #003300;\"><strong>Hi <em>" + username + "</em>,</strong></span></p>\n"
                + "<p><span style=\"color: #003300;\">we will contact you to confirm your registration. Have fun on Maymus.</span></p>\n"
                + "<p><span style=\"color: #003300;\">If you have any questions, just answer this mail.</span></p>\n"
                + "<p><span style=\"color: #003300;\">Greetings,</span><br /><span style=\"color: #003300;\">maymus</span></p>";

        return message;
    }

    private String genNewPassword(String username, String password) {
        String message
                = "<h3>Dear " + username + ",&nbsp;</h3>\n"
                + "<p><span style=\"text-decoration: underline;\">in this email you will find your new password.</span></p>\n"
                + "<p>With the following password and your e-mail address you can now log in. If you want to change the password according to your wishes, you can do this in your account under the point \"Change password\".</p>\n"
                + "<p>Your new password is: " + password + "</p>\n"
                + "<p>&nbsp;</p>\n"
                + "<p>Greetings,</p>\n"
                + "<p>Maymus</p>";

        return message;
    }

    private String genPasswortChangeNotify(String username) {
        String message
                = "<h3>Dear " + username + ",&nbsp;</h3>\n"
                + "<p>you have successfully changed your password. You can now log in to Maymus with your new password.</p>\n"
                + "<p>&nbsp;</p>\n"
                + "<p>Greetings,</p>\n"
                + "<p>Maymus</p>";

        return message;
    }

}
