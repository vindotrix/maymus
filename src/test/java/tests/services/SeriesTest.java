/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import de.maymus.models.Series;
import de.maymus.models.SeriesList;
import de.maymus.services.SeriesListService;
import de.maymus.services.SeriesService;
import de.maymus.util.Constants;
import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.model.tv.TvSeries;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author Kevin
 */
public class SeriesTest {
    
    static final Logger LOG = LoggerFactory.getLogger(SeriesTest.class);
    
    private final SeriesService service;
    private final SeriesListService listService;
    
    private boolean cleanDB = true;
    
    public SeriesTest() {
        LOG.info("=== START SeriesTest ===");
        service = new SeriesService(AbstractDatabaseConnection.getInstance().getDatastore());
        listService = new SeriesListService(AbstractDatabaseConnection.getInstance().getDatastore());
        listService.getAllLists().forEach((s) -> {
            listService.removeSeriesList(s);
        });
        service.getAllSeries().forEach((s) -> {
            service.removeSeries(s);
        });
        System.out.println("");
    }
    
    @Test
    public void getAndSaveSeries() {
        TvSeries series_tmdb = service.getSeriesById(456);
        service.createOrUpdate(new Series(service.getSeriesById(456)));
        Series series_maymus = service.findSeriesInDbById(456);
        
        assertEquals(series_tmdb.getId(), series_maymus.getSeries_id());
        assertEquals(series_tmdb.getName(), series_maymus.getName());
        assertEquals(series_tmdb.getPosterPath(), series_maymus.getPoster());
        assertTrue(series_tmdb.getOverview().length() >= series_maymus.getStory().length());
        assertEquals(series_tmdb.getVoteAverage(), series_maymus.getRating(), 0.001);
        assertEquals(series_tmdb.getFirstAirDate(), series_maymus.getReleaseDate());
        assertEquals(series_tmdb.getStatus(), series_maymus.getStatus());
        assertEquals(series_tmdb.getNumberOfSeasons(), series_maymus.getSeasons());
        assertEquals(series_tmdb.getNumberOfEpisodes(), series_maymus.getEpisodes());
        assertTrue(true);
    }
    
    @Test
    public void remove() {
        service.createOrUpdate(new Series(service.getSeriesById(456)));
        Series series_maymus = service.findSeriesInDbById(456);
        assertNotNull(series_maymus);
        assertTrue(service.removeSeries(series_maymus));
        assertNull(service.findSeriesInDbById(456));
    }
    
    @Test
    public void getAll() {
        service.createOrUpdate(new Series(service.getSeriesById(456)));
        service.createOrUpdate(new Series(service.getSeriesById(615)));
        service.createOrUpdate(new Series(service.getSeriesById(2190)));
        
        assertEquals(3, service.getAllSeries().size());
    }
    
    @Test
    public void test() {
        
        String given = "2007-09-24";
        
        String target1 = "24-09-2007";
        String target2 = "2007";
        String target3 = "-";
        
        String convert1 = TimeHelper.convertUsToDe(given);
        String convert2 = TimeHelper.inputStringToYear(given);
        String convert3 = TimeHelper.convertUsToDe("aölkf3ja90");
        
        assertEquals(target1, convert1);
        assertEquals(target2, convert2);
        assertEquals(target3, convert3);
        System.out.println("");
    }
    
    @Test
    public void seriesx() {
        List<Series> list1 = listService.updateNowPLayingSeries(1);
        list1.addAll(listService.updateNowPLayingSeries(2));
        
        list1.forEach((t) -> {
            service.createOrUpdate(t);
        });
        
        SeriesList x = listService.createOrUpdateList(new SeriesList(list1.subList(0, 40), Constants.NOW_IN_TV, ""));
        assertEquals(40, x.getSeries().size());
    }
}
