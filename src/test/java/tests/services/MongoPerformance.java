/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import de.maymus.models.User;
import de.maymus.services.MovieService;
import de.maymus.services.UserService;
import info.movito.themoviedbapi.model.MovieDb;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author Kevin
 */
public class MongoPerformance {

    static final Logger LOG = LoggerFactory.getLogger(MovieListTest.class);

    Datastore ds;
    private MovieDb movie;
    private MovieService movieService;
    private UserService us;
    private User user;

//CLEAN DATABASE AFTER TEST
    private boolean cleanDB = true;

    @Before
    public void initTest() {
        LOG.info("=== START MovieListTest ===");
        ds = AbstractDatabaseConnection.getInstance().getDatastore();
        us = new UserService(ds);
        movieService = new MovieService(ds);

        if (cleanDB) {
//            mlDbServcie.getAll().forEach((item) -> {
//                mlDbServcie.remove(item);
//            });
            us.getAll().forEach((item) -> {
                us.remove(item);
            });
        }

        user = us.createUser(new User("Maymus", "maymus@maymus.de", "asdlk"));
        if (user == null) {
            user = us.findByUsername("Maymus");
        }
    }

    @Test
    public void vergleich() {
//        List<MovieDb> ml = mlApiService.getPopMovies(1);
//        ml.addAll(mlApiService.getPopMovies(2));
//        ml.addAll(mlApiService.getPopMovies(3));
//        ml.addAll(mlApiService.getPopMovies(4));
//
//        List<Movie> embedded = new LinkedList<>();
//        List<Movie1> reference = new LinkedList<>();
//
//        ml.forEach((item) -> {
//            embedded.add(new Movie(item));
//            reference.add(new Movie1(item));
//        });
//
//        List<Movie1> afterCreate = new LinkedList<>();
//        reference.forEach(
//                (item) -> {
//                    movieService.createOrUpdate(item);
//                }
//        );
//
//        LOG.info("** Start mongo performance test:");
//        LOG.info("** START EMBEDDED: ");
//        long start = timer();
//        MovieList emb = mlDbServcie.createOrUpdateList(new MovieList(user, embedded, "test"));
//        MovieList emb_get = mlDbServcie.findByName(user.getUsername() + ":" + emb.getName());
//        assertEquals(emb.getMovies().size(), emb_get.getMovies().size());
//        long end = timer();
//        long time1 = end - start;
//        LOG.info("** END EMBEDDED!");
//        LOG.info("** Time: " + time1);
//
//        ds.find(MovieList.class).asList().forEach((item) -> {
//            Query<MovieList> query = ds.createQuery(MovieList.class).field("_id").equal(item.getMongo_id());
//            ds.findAndDelete(query);
//        });
//
//        LOG.info("** EMBED FROM COLL");
//        start = timer();
//        List<Movie1> movs = new LinkedList<>();
//        ds.find(Movie1.class).asList().forEach((item) -> {
//            movs.add(item);
//        });
//        user.setWatchlist(movs);
//        user = us.updatUser(user);
//        List<Movie1> aft = user.getWatchlist();
//
//        end = timer();
//        long x = end - start;
//        LOG.info("** END EMBEDDED FROM COLL!");
//        LOG.info("** Time: " + x);
//
//        LOG.info("** START REFERENCED: ");
//        start = timer();
//        afterCreate = ds.find(Movie1.class).asList();
//        MovieList1 ref = mlDbServcie.createOrUpdateList(new MovieList1(user, afterCreate, "test"));
//        MovieList1 ref_get = mlDbServcie.findByIdentifier1(user.getUsername() + ":" + ref.getName());
//        assertEquals(ref.getMovies().size(), ref_get.getMovies().size());
//        end = timer();
//        long time2 = end - start;
//        LOG.info("** END EMBEDDED!");
//        LOG.info("** Time: " + time2);
//
//        LOG.info("****** EMBEDDED  : " + time1);
//        LOG.info("****** REFERENCE : " + time2);
//        LOG.info("****** EMBED FROM COL : " + x);
//
//        ds.find(MovieList1.class).asList().forEach((item) -> {
//            Query<MovieList1> query = ds.createQuery(MovieList1.class).field("_id").equal(item.getUnique_identifier());
//            ds.findAndDelete(query);
//        });
////        ds.find(Movie1.class).asList().forEach((item) -> {
////            Query<Movie1> query = ds.createQuery(Movie1.class).field("_id").equal(item.getMongo_id());
////            ds.findAndDelete(query);
////        });

    }

    private long timer() {
        return System.currentTimeMillis();
    }
}
