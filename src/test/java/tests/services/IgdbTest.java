/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import com.mashape.unirest.http.exceptions.UnirestException;
import de.maymus.models.Game;
import de.maymus.models.api.GameDetail;
import de.maymus.services.GameListService;
import de.maymus.services.GameService;
import de.maymus.util.TimeHelper;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author Kevin
 */
public class IgdbTest {

    GameService gs;
    GameListService gls;

    @Before
    public void init() {
        gs = new GameService(AbstractDatabaseConnection.getInstance().getDatastore());
        gls = new GameListService(AbstractDatabaseConnection.getInstance().getDatastore());
    }

    @Test
    public void saveInMongo() {
//        Game game = gs.createOrUpdate(gs.getGameById(1942));
//        assertNotNull(game);
//        Game game2 = gs.findGameInDb(1942);
//        assertNotNull(game2);
//        gs.removeGame(game2);
//        Game game3 = gs.findGameInDb(1942);
//        assertNull(game3);

    }

    @Test
    public void test() throws UnirestException {

        //List<GameIGDB> list = wrapper.getComingSoon();
        //System.out.println("Liste: " + list);
    }

    @Test
    public void x() {
        Long x = 1391636804580L;
        System.out.println(TimeHelper.getDateFromTimestamp(x));
    }

    @Test
    public void getListAndStoreInDatabase() {
//        assertEquals(0, gls.getAllLists().size());
//        gls.updateTopRatedGames();
//        assertEquals(1, gls.getAllLists().size());
//        GameList list = gls.findGameListByName(Constants.UPCOMMING);
//        assertNotNull(list);
//        assertEquals(40, list.getGames().size());
//        gls.removeGameList(list);
//        assertEquals(0, gls.getAllLists().size());
    }

    @Test
    public void manipulateList() {
//        Game game1 = gs.getGameById(1942);
//        Game game2 = gs.getGameById(1943);
//
//        GameList list = new GameList("test", "desc", true, new LinkedList<>());
//
//        assertEquals(0, list.getGames().size());
//
//        list.addGame(list, game1);
//        assertEquals(1, list.getGames().size());
//
//        list.addGame(list, game1);
//        assertEquals(1, list.getGames().size());
//
//        list.addGame(list, game2);
//        assertEquals(2, list.getGames().size());
//
//        list.addGame(list, game2);
//        assertEquals(2, list.getGames().size());
//
//        list.removeGame(list, game1);
//        assertEquals(1, list.getGames().size());
//
//        list.removeGame(list, game2);
//        assertEquals(0, list.getGames().size());
//
//        list.addGame(list, game2);
//        assertEquals(1, list.getGames().size());

    }

    @Test
    public void updateUpcomming() {
        //gls.updateUpcommingGAmes();

    }

    @Test
    public void igdbExpand() {
        GameDetail game = gs.getGameIgdbDetailById(1942);
        //GameIGDB gameOld = gs.getGameIgdbById(1942);

        List<Integer> list = new LinkedList<>();
        game.getReleaseDates().forEach((t) -> {
            list.add(t.getPlatform());
        });
        Map<Integer, String> plat = new HashMap<>();
        plat = gs.getGameIgdbReleaseDates(list);
        System.out.println("s");
    }

    @Test
    public void igdbMaymusLists() {
        List<Game> sl = gls.recentlyReleasedList();
        System.out.println("");
    }

    @Test
    public void testRating() throws ParseException {
        DecimalFormat df2 = new DecimalFormat(".#");
        double input = 32.123456;
        double fin = 32.1;

        String x2 = df2.format(input);
        double x3 = Double.parseDouble(x2.replaceAll(",", "."));

        NumberFormat nf = NumberFormat.getInstance();
        double x4 = nf.parse(x2).doubleValue();

        assertEquals(fin, 0.001, x3);
        assertEquals(fin, 0.001, x4);
    }

}
