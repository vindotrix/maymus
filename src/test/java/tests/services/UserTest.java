/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import com.mongodb.DuplicateKeyException;
import de.maymus.beans.SessionBean;
import de.maymus.handler.ResourceHandler;
import de.maymus.models.User;
import de.maymus.services.UserService;
import java.util.Iterator;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author vindotrix
 */
public class UserTest {
    
    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SessionBean.class);
    
    private UserService us;

    //CLEAN DATABASE AFTER TEST
    private final boolean cleanDbAfterTest = true;
    
    @Before
    public void DatabaseTest() {
        Datastore ds = AbstractDatabaseConnection.getInstance().getDatastore();
        us = new UserService(ds);
        LOG.info("=== START DATABASTEST ===");
        LOG.info(ds.getMongo().getUsedDatabases().toString());
        
        if (cleanDbAfterTest) {
            us.getAll().forEach((mo) -> {
                us.remove(mo);
            });
        }
        
    }
    
    @Test
    public void createOneUser() {
        LOG.info("=== Start 'createOrUpdateOneUser' ===");
        
        String username = "oneUser";
        LOG.info("Try to create USer: " + username);
        User user = new User(username, "test@one.io", "testCreateUserWithoutCredential");
        LOG.info("User created true: " + (user.getId() == null));
        assertNull(user.getId());
        us.createUser(user);
        LOG.info("User created true: " + (user.getId() != null));
        assertNotNull(user.getId());
    }
    
    @Test
    public void duplicateUsernameTest() {
        LOG.info("=== Start 'duplicateUsernameTest' ===");
        
        String username = "duplicateUserName";
        
        LOG.info("Try to create User1");
        User user1 = us.createUser(new User(username, "username1@username.io", "duplicateUserNameXX_!@"));
        LOG.info("True: " + (user1 != null));
        assertNotNull(user1);
        
        LOG.info("Try to create User2 with same name.");
        User user2 = us.createUser(new User(username, "username2@username.io", "duplicateUserNameXX_!@"));
        LOG.info("True: " + (user2 == null));
        assertNull(user2);
        
    }
    
    @Test
    public void duplicateEmailTest() {
        LOG.info("=== Start 'duplicateEmailTest' ===");
        String email = "email@email.io";
        
        LOG.info("Try to create User1");
        User user1 = us.createUser(new User("username1", email, "12345ABCD"));
        LOG.info("True: " + (user1 != null));
        assertNotNull(user1);
        
        LOG.info("Try to create User2");
        User user2 = us.createUser(new User("username2", email, "12345ABCD"));
        LOG.info("True: " + (user2 == null));
        assertNull(user2);
        
    }
    
    @Test
    public void findUserByUsername() {
        LOG.info("=== Start 'removeOneByUsername' ===");
        String username = "findByUSername";
        User user1 = us.createUser(new User(username, "findUserName@username.io", "duplicateUserNameXX_!@"));
        assertNotNull(user1);
        
        User foundUser = us.findByUsername(username);
        assertEquals(user1.getId(), foundUser.getId());
        
        User x = us.findByUsername("noUser");
        LOG.info("Assert null: " + x);
        assertNull(x);
    }
    
    @Test
    public void removeOneByUsername() {
        LOG.info("=== Start 'removeOneByUsername' ===");
        
        User user1 = us.createUser(new User("removeOnebyUsSer", "username1@remove.io", "duplicateUserNamlsdkjfeXX_!@"));
        assertNotNull(user1);
        assertNotNull(user1.getId());
        
        User found = us.findUserById(user1.getId());
        assertNotNull(found);
        
        assertTrue(us.remove(found));
        assertFalse(us.remove(new User("alsdkfj", "alsdfkj", "adlskfj")));
        
    }
    
    @After
    public void clear() {
        us.getAll().forEach((item) -> {
            us.remove(item);
        });
    }
    
}
