/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import de.maymus.services.MovieService;
import de.maymus.services.MovieListService;
import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.model.MovieDb;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
public class MoviesOnlineTest {

    static final Logger LOG = LoggerFactory.getLogger(MoviesOnlineTest.class);
    private final MovieService mso;
    private final MovieListService mlApiService;

    //CLEAN DATABASE AFTER TEST
    public MoviesOnlineTest() {
        mso = new MovieService();
        mlApiService = new MovieListService();
    }

    @Test
    public void getTopMovies() {
        LOG.info("==== START 'getPopMovies' ====");

//        List<MovieDb> list = mlApiService.getPopMovies(1);
//        LOG.info("List size: " + list.size());
//        assertEquals(list.size(), 20);
    }

    public void testx() {
        String test = "Erster Satz. Zweiter Satz. Dritter Satz. Vierter Satz. Fünfter Satz";
        String result = "";

        while (test.length() > 45) {
            if (test.length() > 45) {
                String[] x = test.split(Pattern.quote("."));
                String tmp = "";

                for (int i = 0; i < x.length - 1; i++) {
                    tmp = tmp.concat(x[i]).concat(".");
                }
                result = tmp;
            }
        }

        LOG.info("Given String: " + test);
        LOG.info("Grenze: " + test.substring(0, 42));
        LOG.info("Soll: " + test.substring(0, 40));
        LOG.info("Ist: " + result);
        assertEquals(test.substring(0, 45), result);

    }

}
