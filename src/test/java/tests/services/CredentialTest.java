/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import de.maymus.models.User;
import de.maymus.services.CredentialService;
import de.maymus.services.UserService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author Kevin
 */
public class CredentialTest {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(CredentialTest.class);

    CredentialService cs = new CredentialService();
    private final UserService us;

    public CredentialTest() {
        us = new UserService(AbstractDatabaseConnection.getInstance().getDatastore());
    }

    @Test
    public void userCredentialTestWithoutDatabase() {
        LOG.info("=== Start 'userCredentialTestWithoutDatabase' ===");

        String password = "I'binD@Passw0rtd_";
        String hash = cs.createUserPassword(password);
        User user = new User("username", "username@maymus.de", hash);
        LOG.info("Created User: " + user.toString());
        LOG.info("Hash: " + user.getPassword());

        boolean match = cs.validate(password, hash);
        LOG.info("Assert True: " + match);
        assertTrue(match);

        match = cs.validate(password.concat("."), hash);
        LOG.info("Assert False: " + match);
        assertFalse(match);

        match = cs.validate(password, hash);
        LOG.info("Assert True: " + match);
        assertTrue(match);
    }

    @Test
    public void userCredentialTestWithDatabase() {
        LOG.info("=== Start 'userCredentialTestWithDatabase' ===");
        String password = "I#B!nDäPrasident_";
        String hash = cs.createUserPassword(password);
        User user = new User("El'Präsidente", "präsi@gov.gov", hash);
        LOG.info("Created User: " + user.toString());
        LOG.info("Hash: " + user.getPassword());
        LOG.info("Try to save User: " + user.toShortString());
        User foundUser = us.createUser(user);
        if (foundUser != null) {
            boolean match = cs.validate(password, foundUser.getPassword());
            LOG.info("Assert True: " + match);
            assertTrue(match);

            match = cs.validate(password.concat("."), foundUser.getPassword());
            LOG.info("Assert False: " + match);
            assertFalse(match);

            match = cs.validate(password, foundUser.getPassword());
            LOG.info("Assert True: " + match);
            assertTrue(match);

            us.remove(foundUser);
        } else {
            LOG.info("Any error occured in Test: User could not be saved or found");
            assertTrue(false);
        }

    }

}
