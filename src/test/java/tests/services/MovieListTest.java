/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.services;

import de.maymus.rest.TmdbService;
import de.maymus.models.Movie;
import de.maymus.models.MovieList;
import de.maymus.models.User;
import de.maymus.services.CredentialService;
import de.maymus.services.MovieListService;
import de.maymus.services.MovieService;
import de.maymus.services.UserService;
import de.maymus.util.Constants;
import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.Discover;
import info.movito.themoviedbapi.model.MovieDb;
import java.util.LinkedList;
import java.util.List;
import java.util.function.UnaryOperator;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author Kevin
 */
public class MovieListTest {

    static final Logger LOG = LoggerFactory.getLogger(MovieListTest.class);

    Datastore ds;
    private MovieDb movie;
    private MovieService mApiService;
    private MovieListService mlApiService;
    private CredentialService cs;
    private UserService us;
    private User user;
    private final String popular = "popular";
    private final String upcomming = "upcomming";
    private List<MovieDb> tmdbList = new LinkedList<>();

//CLEAN DATABASE AFTER TEST
    private boolean cleanDB = true;

    @Before
    public void initTest() {
        LOG.info("=== START MovieListTest ===");
        ds = AbstractDatabaseConnection.getInstance().getDatastore();
        us = new UserService(ds);
        cs = new CredentialService();
        mApiService = new MovieService(ds);
        mlApiService = new MovieListService();

    }

    @Test
    public void newx() {
        List<Movie> pop = new LinkedList<>();
        List<Movie> top = new LinkedList<>();
        List<Movie> next = new LinkedList<>();
        List<Movie> now = new LinkedList<>();

//        for (int i = 1; i < 3; i++) {
//            //------------- Most popular
//            mlApiService.getPopMovies(i).forEach((item) -> {
//                pop.add((new Movie(item)));
//            });
//
//            //------------ Top rated
//            mlApiService.updateTopRated(i).forEach((item) -> {
//                top.add((new Movie(item)));
//            });
//
//            //------------- Upcomming
//            mlApiService.updateUpcomming(i).forEach((item) -> {
//                next.add((new Movie(item)));
//            });
//
//            //-------------- Now playing
//            mlApiService.updateNowCinema(i).forEach((item) -> {
//                now.add((new Movie(item)));
//            });
//        }
        MovieList topList = new MovieList(top, Constants.TOP_RATED, "", false);
        MovieList nextList = new MovieList(next, Constants.UPCOMMING, "", false);
        MovieList nowList = new MovieList(now, Constants.NOW_IN_CINEMA, "", false);

        mlApiService.createOrUpdateList(nextList);
        mlApiService.createOrUpdateList(topList);
        mlApiService.createOrUpdateList(nowList);

        int before = pop.size();

        assertEquals(before, 40);

    }

    @Test
    public void aödklsfj() {
        Movie mov1 = new Movie(mApiService.getMovieById(211672));
        Movie mov2 = new Movie(mApiService.getMovieById(321612));
        Movie mov3 = new Movie(mApiService.getMovieById(396422));
        Movie mov4 = new Movie(mApiService.getMovieById(339846));
        Movie mov5 = new Movie(mApiService.getMovieById(390043));

        mApiService.createOrUpdate(mov1);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov3);
        mApiService.createOrUpdate(mov4);
        mApiService.createOrUpdate(mov5);

        List<Movie> movs = new LinkedList<>();
        movs.add(mov1);
        movs.add(mov2);
        movs.add(mov3);
        movs.add(mov4);
        movs.add(mov5);
        MovieList list = new MovieList(movs, "TEST", "", false);
        list = mlApiService.createOrUpdateList(list);

        assertEquals(5, list.getMovies().size());

        list.getMovies().remove(4);
        list = mlApiService.createOrUpdateList(list);
        assertEquals(4, list.getMovies().size());

        list.addMovie(list, mov1);
        list = mlApiService.createOrUpdateList(list);
        assertEquals(4, list.getMovies().size());

        list.addMovie(list, mov5);
        list = mlApiService.createOrUpdateList(list);
        assertEquals(5, list.getMovies().size());

    }

    @Test
    public void aldkfj() {
        Movie mov1 = new Movie(mApiService.getMovieById(211672));
        Movie mov2 = new Movie(mApiService.getMovieById(321612));
        Movie mov3 = new Movie(mApiService.getMovieById(396422));
        Movie mov4 = new Movie(mApiService.getMovieById(339846));
        Movie mov5 = new Movie(mApiService.getMovieById(390043));

        mApiService.createOrUpdate(mov1);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov3);
        mApiService.createOrUpdate(mov4);
        mApiService.createOrUpdate(mov5);

        long start = System.currentTimeMillis();
        List<Movie> movs = new LinkedList<>();
        for (int i = 0; i < 10000; i++) {
            movs.add(mov1);
        }
        MovieList list = new MovieList(movs, "fehler", "", false);
        list = mlApiService.createOrUpdateList(list);

        long midd = System.currentTimeMillis();
        Movie get = null;
        String x = "";
        for (Movie mov : mlApiService.findMovieListByName("fehler").getMovies()) {
            get = mov;
            x = get.getStory();

        }
        long end = System.currentTimeMillis();

        LOG.info("1 Sektor: " + (midd - start));
        LOG.info("2 Sektor: " + (end - midd));
        LOG.info("Gesamt : " + (end - start));

    }

    @Test
    public void xx() {
        List<Movie> top = new LinkedList<>();
        List<Movie> next = new LinkedList<>();
        List<Movie> now = new LinkedList<>();

//        for (int i = 1; i < 3; i++) {
//
//            //------------ Top rated
//            mlApiService.updateTopRated(i).forEach((item) -> {
//                top.add(mApiService.createOrUpdate(new Movie(item)));
//            });
//
//            //------------- Upcomming
//            mlApiService.updateUpcomming(i).forEach((item) -> {
//                next.add(mApiService.createOrUpdate(new Movie(item)));
//            });
//
//            //-------------- Now playing
//            mlApiService.updateNowCinema(i).forEach((item) -> {
//                now.add(mApiService.createOrUpdate(new Movie(item)));
//            });
//        }
        MovieList topList = new MovieList(top, Constants.TOP_RATED, "", false);
        MovieList nextList = new MovieList(next, Constants.UPCOMMING, "", false);
        MovieList nowList = new MovieList(now, Constants.NOW_IN_CINEMA, "", false);
        mlApiService.createOrUpdateList(topList);
        mlApiService.createOrUpdateList(nextList);
        mlApiService.createOrUpdateList(nowList);
    }

    @Test
    public void userList() {
//        User admin = us.findByUsername("Maymus");
//        if (admin == null) {
//            us.createUser(new User("Maymus", "maymus@maymus.de", cs.createUserPassword("Test123")));
//        }
//        Movie mov1 = new Movie(mApiService.getMovieById(211672));
//        Movie mov2 = new Movie(mApiService.getMovieById(321612));
//        Movie mov3 = new Movie(mApiService.getMovieById(396422));
//        Movie mov4 = new Movie(mApiService.getMovieById(339846));
//        Movie mov5 = new Movie(mApiService.getMovieById(390043));
//
//        mApiService.createOrUpdate(mov1);
//        mApiService.createOrUpdate(mov2);
//        mApiService.createOrUpdate(mov3);
//        mApiService.createOrUpdate(mov4);
//        mApiService.createOrUpdate(mov5);
//
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov1));
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov1));
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov3));
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov1));
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov5));
//        admin.setWishlist(us.addMovieToUserList(admin.getWishlist(), mov2));
//        us.updatUser(admin);
//
//        assertEquals(4, us.findByUsername("Maymus").getWishlist().size());
//
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov3));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov5));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov2));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov4));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov3));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov4));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov5));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov3));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov5));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov2));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov4));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov3));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov4));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov5));
//        admin.setWatchlist(us.addMovieToUserList(admin.getWatchlist(), mov1));
//        us.updatUser(admin);
//        assertEquals(5, us.findByUsername("Maymus").getWatchlist().size());

    }

    @Test
    public void multipleUserListsChoose() {

        User admin = us.findByUsername("Maymus");
        if (admin == null) {
            admin = us.createUser(new User("Maymus", "maymus@maymus.de", cs.createUserPassword("Test123")));
        }
        Movie mov1 = new Movie(mApiService.getMovieById(211672));

        mApiService.createOrUpdate(mov1);
        assertEquals(us.findByUsername("Maymus").getMovieWatchlist().getMovies().size(), 0);

        MovieList list = admin.getMovieWatchlist();

        list.getMovies().add(mov1);

        us.updateUser(admin);
        assertEquals(us.findByUsername("Maymus").getMovieWatchlist().getMovies().size(), 1);

    }

    @Test
    public void customMovieLists() {

        User admin = us.findByUsername("Maymus");
        if (admin != null) {
            us.remove(admin);
        }

        us.createUser(new User("Maymus", "maymus@maymus.de", cs.createUserPassword("Test123")));
        admin = null;
        admin = us.findByUsername("Maymus");

        Movie mov1 = new Movie(mApiService.getMovieById(211672));
        Movie mov2 = new Movie(mApiService.getMovieById(321612));
        Movie mov3 = new Movie(mApiService.getMovieById(396422));
        Movie mov4 = new Movie(mApiService.getMovieById(339846));
        Movie mov5 = new Movie(mApiService.getMovieById(390043));

        mApiService.createOrUpdate(mov1);
        mApiService.createOrUpdate(mov2);
        mApiService.createOrUpdate(mov3);
        mApiService.createOrUpdate(mov4);
        mApiService.createOrUpdate(mov5);

        List<Movie> list1 = new LinkedList<>();
        list1.add(mov1);
        list1.add(mov2);
        list1.add(mov3);

        List<Movie> list2 = new LinkedList<>();
        list2.add(mov4);
        list2.add(mov5);
        list2.add(mov3);
        list2.add(mov1);
        list2.add(mov2);

        MovieList movieList1 = new MovieList(list1, "list1", "", false);
        MovieList movieList2 = new MovieList(list2, "list2", "", false);

        admin.getMovieCustomLists().add(movieList1);
        admin.getMovieCustomLists().add(movieList2);

        admin = us.updateUser(admin);

        for (MovieList list : admin.getMovieCustomLists()) {
            if (list.getName().equals("list1")) {
                assertEquals(3, list.getMovies().size());
            }
            if (list.getName().equals("list2")) {
                assertEquals(5, list.getMovies().size());
            }
        }

        assertNotNull(admin.getMovieCustomLists().get(0));
        assertNotNull(admin.getMovieCustomLists().get(1));
        admin = us.findByUsername("Maymus");
        for (MovieList list : admin.getMovieCustomLists()) {
            if (list.getName().equals("list1")) {
                list.getMovies().remove(mov2);
            }
            if (list.getName().equals("list2")) {
                list.getMovies().remove(mov3);
                list.getMovies().remove(mov5);
            }
        }
        User user = us.updateUser(admin);

        for (MovieList list : user.getMovieCustomLists()) {
            if (list.getName().equals("list1")) {
                assertEquals(2, list.getMovies().size());
            }
            if (list.getName().equals("list2")) {
                assertEquals(3, list.getMovies().size());
            }
        }
    }
}
