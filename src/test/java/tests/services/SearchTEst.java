/*
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tests.services;

import de.maymus.models.Game;
import de.maymus.models.Movie;
import de.maymus.models.Series;
import static de.maymus.rest.TmdbService.getInstance;
import de.maymus.services.GameService;
import de.maymus.services.MovieService;
import de.maymus.services.SeriesService;
import info.movito.themoviedbapi.TmdbSearch;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tests.abstracts.AbstractDatabaseConnection;

/**
 *
 * @author brach.kevin@stud.htwk-leipzig.de
 */
public class SearchTEst {

    static final Logger LOG = LoggerFactory.getLogger(MoviesOnlineTest.class);
    private final Datastore ds;
    private final SeriesService seriesService;
    private final GameService gameService;
    private final MovieService movieService;

    public SearchTEst() {
        ds = AbstractDatabaseConnection.getInstance().getDatastore();
        seriesService = new SeriesService(ds);
        gameService = new GameService(ds);
        movieService = new MovieService(ds);
    }

    @Test
    public void testSearchMovie() {
        // Try a movie with less than 1 page of results

        List<Movie> list = movieService.searchMovies("Star Wars");
        List<Series> list2 = seriesService.searchTv("Star Wars");
        List<Game> list3 = gameService.searchGame("Star wars");
        Assert.assertNotNull(list3);
        Assert.assertNotNull(list2);
        Assert.assertNotNull(list);
        System.out.println("");

    }

}
