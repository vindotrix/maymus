/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.dataaccess.UserDao;
import de.maymus.models.Movie;
import de.maymus.models.User;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
public class UserService {

    static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final UserDao userDao;
    private CredentialService cs;

    public UserService() {
        LOG.info("* Start new userService *");
        this.userDao = new UserDao();
        cs = new CredentialService();
    }

    public UserService(Datastore das) {
        this.userDao = new UserDao(das);
        cs = new CredentialService();
    }

    public User createUser(User user) {
        User ret = userDao.createUser(user);
        String log = ret != null ? "User wurde erstellt!" : "User wurde nicht!";
        LOG.info(log);
        return ret;

    }

    public User updateUser(User user) {
        User ret = userDao.updateUser(user);
        String log = ret != null ? "User wurde aktualisiert!" : "User wurde nicht aktualisiert!";
        LOG.info(log);
        return ret;
    }

    public User findByUsername(String username) {
        User ret = userDao.findUserByUsername(username);
        String log = ret != null ? "User wurde gefunden!" : "User wurde nicht gefunden!";
        LOG.info(log);
        return ret;

    }

    public User findUserById(Object id) {
        User ret = userDao.findUserById(id);
        String log = ret != null ? "User wurde gefunden!" : "User wurde nicht gefunden!";
        LOG.info(log);
        return ret;

    }

    public User findUserByEMail(String mail) {
        User ret = userDao.findUserByMail(mail);
        String log = ret != null ? "User wurde gefunden!" : "User wurde nicht gefunden!";
        LOG.info(log);
        return ret;

    }

    public boolean remove(User username) {
        Boolean ret = userDao.remove(username);
        String log = ret ? "User wurde gelöscht!" : "User wurde nicht gelöscht!";
        LOG.info(log);
        return ret;

    }

    public List<User> getAll() {
        List<User> ret = userDao.getAllUser();
        String log = ret.size() > 0 ? ret.size() + " User wurden gefunden" : "Keine User in der Datenbank";
        LOG.info(log);
        return ret;

    }

    public User login(String email, String password) {
        User user = null;
        try {
            user = findUserByEMail(email);
            LOG.info("Validate Passwrord: " + user.getUsername());
            boolean valid = cs.validate(password, user.getPassword());
            LOG.info("Is Password valid: " + valid);
            if (!valid) {
                user = null;
            }
        } catch (NullPointerException e) {
            LOG.error("Nullpointerexception: " + e);
        }
        return user;
    }

    public boolean signUp(String username, String email, String password) {
        password = cs.createUserPassword(password);
        User newUser = createUser(new User(username, email, password));
        return newUser != null;
    }

    public boolean verifyPassword(String password, String hash) {
        return cs.validate(password, hash);
    }

    public User changePassword(String password, User user) {
        password = cs.createUserPassword(password);
        user.setPassword(password);
        return updateUser(user);
    }

    public User changePassword(String password, String email) {
        password = cs.createUserPassword(password);
        User user = findUserByEMail(email);
        if (user != null) {
            user.setPassword(password);
            return updateUser(user);
        } else {
            return null;
        }
    }
}
