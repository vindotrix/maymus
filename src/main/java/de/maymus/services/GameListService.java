/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.IgdbService;
import de.maymus.dataaccess.GameListDao;
import de.maymus.models.Game;
import de.maymus.models.GameList;
import static de.maymus.services.GameListService.LOG;
import de.maymus.util.Constants;
import java.util.LinkedList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.mongodb.morphia.Datastore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class GameListService extends IgdbService {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GameListService.class);

    private GameListDao gsd;
    private Datastore gsds;

    public GameListService() {
        LOG.info("** Start new gameListService **");
        gsd = new GameListDao();
    }

    public GameListService(Datastore ds) {
        LOG.info("** Start new TESTgameListService **");
        gsd = new GameListDao(ds);
        gsds = ds;
    }

    //DATABASE
    public GameList findGameListByName(String name) {
        GameList ret = gsd.findGameListByName(name);
        String log = ret != null ? "List Found!" : "List NOT Found!";
        LOG.info(log);
        return ret;
    }

    public boolean removeGameList(GameList ml) {
        boolean ret = gsd.remove(ml);
        String log = ret ? "Successfully deleted!" : "NOT deleted!";
        LOG.info(log);
        return ret;
    }

    public GameList createOrUpdateList(GameList list) {
        GameList ret = gsd.createOrUpdateList(list);
        String log = ret != null ? "CreateOrUpdate Successfull!" : "CreateOrUpdate NOT Successfull!";
        LOG.info(log);
        return ret;
    }

    public List<GameList> getAllLists() {
        return gsd.getAll();
    }

    public GameService singleService() {
        if (gsds != null) {
            return new GameService(gsds);
        }
        return new GameService();
    }

    public List<Game> updateTopLists() {
        List<Game> ret = new LinkedList<>();
        getTopList().forEach((t) -> {
            ret.add((new Game(t)));
        });
        return ret;
    }

    public List<Game> recentlyReleasedList() {
        List<Game> ret = new LinkedList<>();
        getNewList().forEach((t) -> {
            ret.add((new Game(t)));
        });

        return ret;
    }

    public List<Game> updateUpcomming() {
        List<Game> ret = new LinkedList<>();
        getUpcommingList().forEach((t) -> {
            ret.add((new Game(t)));
        });
        return ret;
    }
}
