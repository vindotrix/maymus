/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.TmdbService;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.TmdbTV;
import info.movito.themoviedbapi.model.Artwork;
import info.movito.themoviedbapi.model.Credits;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.MovieImages;
import info.movito.themoviedbapi.model.Video;
import info.movito.themoviedbapi.model.tv.TvSeries;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class TmdbDetailService extends TmdbService {

    static final Logger LOG = LoggerFactory.getLogger(TmdbDetailService.class);

    private final transient TmdbMovies movies;
    private final transient TmdbTV series;
    private final String POSTERPATH_DEFAULT = "/resources/poster.jpg";

    public TmdbDetailService() {
        LOG.info("* Start new TmdbDetailService *");

        movies = getInstance().getTmdb().getMovies();
        series = getInstance().getTmdb().getTvSeries();
    }

    public Credits getCredits(int id) {
        return movies.getCredits(id);
    }

    public MovieImages getImages(int id) {
        return movies.getImages(id, "de");
    }

    public List<Video> gets(int id) {
        return movies.getVideos(id, "de");
    }

    public List<MovieDb> getSimilar(int id) {
        return movies.getSimilarMovies(id, "de", 1).getResults();
    }

}
