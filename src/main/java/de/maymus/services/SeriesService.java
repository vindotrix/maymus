/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.TmdbService;
import de.maymus.dataaccess.SeriesDao;
import de.maymus.models.Series;
import static de.maymus.services.MovieService.LOG;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.TmdbSearch;
import info.movito.themoviedbapi.TmdbTV;
import info.movito.themoviedbapi.TmdbTvSeasons;
import info.movito.themoviedbapi.Utils;
import info.movito.themoviedbapi.model.core.ResponseStatusException;
import info.movito.themoviedbapi.model.tv.TvSeason;
import info.movito.themoviedbapi.model.tv.TvSeries;
import java.util.LinkedList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class SeriesService extends TmdbService {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SeriesService.class);
    private final transient TmdbTV tmdbTv;

    private final SeriesDao seriesDao;

    public SeriesService() {
        LOG.info("*** Start new SeriesService ***");
        this.seriesDao = new SeriesDao();
        tmdbTv = getInstance().getTmdb().getTvSeries();
    }

    public SeriesService(Datastore ds) {
        LOG.info("*** Start new SeriesService ***");
        this.seriesDao = new SeriesDao(ds);
        tmdbTv = getInstance().getTmdb().getTvSeries();

    }

    public TvSeries getSeriesById(int id) {
        return getSeriesById(id, "de");
    }

    public TvSeries getSeriesById(int id, String lang) {
        LOG.info("Try to get Series by id: " + id);
        TvSeries show = null;
        try {
//            lang = (lang = null) ? "de" : lang;
            show = tmdbTv.getSeries(id, "de", TmdbTV.TvMethod.credits, TmdbTV.TvMethod.videos, TmdbTV.TvMethod.recommendations, TmdbTV.TvMethod.external_ids);
        } catch (ResponseStatusException e) {
            LOG.info("Exception getting TV Show. Code: {}, Message: {}", e.getResponseStatus(), e.getMessage());
        }
        if (show != null) {
            LOG.info("Got Sho: " + show.getId() + ", " + show);
        } else {
            LOG.info("No Show found for given id: " + id);
        }
        return show;
    }

    public List<TvSeason> getAllSeasonsToSeries(int id, int count) {
        LOG.info("Try to get Seasons to Series by id: " + id);
        TmdbTvSeasons tmdbSeasons = getTmdb().getTvSeasons();
        List<TvSeason> list = new LinkedList<>();
        try {
            for (int i = 0; i < count; i++) {
                list.add(tmdbSeasons.getSeason(id, count, "de", TmdbTvSeasons.SeasonMethod.images, TmdbTvSeasons.SeasonMethod.videos, TmdbTvSeasons.SeasonMethod.credits));
            }
        } catch (Exception e) {

        }
        return list;
    }

    public TvSeason getSeason(int series, int season) {
        LOG.info("Try to get Season: " + season + " to Series by id: " + series);
        TmdbTvSeasons tmdbSeasons = getTmdb().getTvSeasons();
        return (tmdbSeasons.getSeason(series, season, "de", TmdbTvSeasons.SeasonMethod.images, TmdbTvSeasons.SeasonMethod.videos, TmdbTvSeasons.SeasonMethod.credits));
    }

    public List<Series> searchTv(String keyword) {
        List<Series> list = new LinkedList<>();
        getTmdb().getSearch().searchTv(keyword, "de", 1).getResults().forEach((TvSeries t) -> {
            list.add((new Series(t)));
        });
        return list;
    }

    //DB SERVICES
    public Series findSeriesInDbById(int id) {
        return seriesDao.findById(id);
    }

    public boolean removeSeries(Series series) {
        return seriesDao.remove(series);
    }

    public List<Series> getAllSeries() {
        return seriesDao.getAll();
    }

    public Series createOrUpdate(Series series) {
        return seriesDao.createOrUpdate(series);
    }
}
