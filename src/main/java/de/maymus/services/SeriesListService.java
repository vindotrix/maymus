/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.dataaccess.SeriesListDao;
import de.maymus.models.Series;
import de.maymus.models.SeriesList;
import de.maymus.rest.TmdbService;
import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbTV;
import java.util.LinkedList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class SeriesListService extends TmdbService {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SeriesListService.class);

    private final transient TmdbTV series;
    private final transient TmdbApi api;
    final SeriesListDao dao;
    final Datastore dss;

    public SeriesListService() {
        LOG.info("** Start new SeriesListService **");
        series = getInstance().getTmdb().getTvSeries();
        api = getInstance().getTmdb();
        dao = new SeriesListDao();
        dss = null;
    }

    public SeriesListService(Datastore ds) {
        LOG.info("** Start new SeriesListService **");
        series = getInstance().getTmdb().getTvSeries();
        api = getInstance().getTmdb();
        dao = new SeriesListDao(ds);
        dss = ds;
    }

    public List<Series> updateNowPLayingSeries(int x) {
        List<Series> ret = new LinkedList<>();
        series.getOnTheAir("de", x).forEach((t) -> {
            ret.add(new Series(t));
        });
        return ret;
    }

    public List<Series> updateTopSeries(int x) {
        List<Series> ret = new LinkedList<>();
        series.getTopRated("de", x).forEach((t) -> {
            ret.add(new Series(t));
        });
        return ret;

    }

    //DATABASE
    public SeriesList findSeriesListByName(String name) {
        SeriesList ret = dao.findMovieListByName(name);
        String log = ret != null ? "List Found!" : "List NOT Found!";
        LOG.info(log);
        return ret;
    }

    public boolean removeSeriesList(SeriesList ml) {
        boolean ret = dao.remove(ml);
        String log = ret ? "Successfully deleted!" : "NOT deleted!";
        LOG.info(log);
        return ret;
    }

    public SeriesList createOrUpdateList(SeriesList list) {
        SeriesList ret = dao.createOrUpdateList(list);
        String log = ret != null ? "CreateOrUpdate Successfull!" : "CreateOrUpdate NOT Successfull!";
        LOG.info(log);
        return ret;
    }

    public List<SeriesList> getAllLists() {
        return dao.getAll();
    }

}
