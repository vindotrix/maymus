/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.IgdbService;
import de.maymus.dataaccess.GameDao;
import de.maymus.models.Game;
import de.maymus.models.api.GameDetail;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@ApplicationScoped
public class GameService extends IgdbService {

    static final Logger LOG = LoggerFactory.getLogger(GameService.class);

    private final GameDao gameDao;

    public GameService() {
        gameDao = new GameDao();
    }

    public GameService(Datastore ds) {
        gameDao = new GameDao(ds);
    }

    public GameDetail getGameIgdbDetailById(int id) {
        return igdb_game_detail(id);
    }

    public Map<Integer, String> getGameIgdbReleaseDates(List<Integer> id) {
        return igdb_game_platfroms(id);
    }

    public List<Game> searchGame(String keyword) {
        List<Game> list = new LinkedList<>();
        getSearchGame(keyword).forEach((GameDetail t) -> {
            list.add((new Game(t)));
        });
        return list;
    }

    ///DATABASE
    public Game findGameInDb(int id) {
        return gameDao.findById(id);
    }

    public boolean removeGame(Game game) {
        return gameDao.remove(game);
    }

    public List<Game> getAllGames() {
        return gameDao.getAll();
    }

    public Game createOrUpdate(Game game) {
        return gameDao.createOrUpdate(game);
    }

}
