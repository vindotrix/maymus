/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.TmdbService;
import de.maymus.dataaccess.MovieDao;
import de.maymus.models.Movie;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.Utils;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.ResponseStatusException;
import java.util.LinkedList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
public class MovieService extends TmdbService {

    static final Logger LOG = LoggerFactory.getLogger(MovieService.class);

    private final transient TmdbMovies movies;

    private final MovieDao movieDao;

    public MovieService() {
        LOG.info("*** Start new MovieService ***");
        this.movieDao = new MovieDao();

        movies = getInstance().getTmdb().getMovies();
    }

    public MovieService(Datastore das) {
        LOG.info("*** Start new MovieService ***");
        this.movieDao = new MovieDao(das);
        movies = getInstance().getTmdb().getMovies();

    }

    public MovieDb getMovieById(int id) {
        return getMovieById(id, "de");
    }

    public MovieDb getMovieById(int id, String lang) {
        LOG.info("Try to get Movie by id: " + id);
        MovieDb movie = null;
        try {
            lang = (lang == null) ? "de" : lang;
            movie = movies.getMovie(id, "de", TmdbMovies.MovieMethod.keywords, TmdbMovies.MovieMethod.videos, TmdbMovies.MovieMethod.similar, TmdbMovies.MovieMethod.credits);

        } catch (ResponseStatusException e) {
            LOG.info("Exception getting Movie. Code: {}, Message: {}", e.getResponseStatus(), e.getMessage());
        }
        if (movie != null) {
            LOG.info("Got Movie: " + movie.getId() + ", " + movie);
        } else {
            LOG.info("No Movie found for given id: " + id);
        }
        return movie;
    }

    public List<Movie> searchMovies(String keyword) {
        List<Movie> list = new LinkedList<>();
        getTmdb().getSearch().searchMovie(keyword, 1, "", true, 1).getResults().forEach((MovieDb t) -> {
            list.add((new Movie(t)));
        });
        return list;
    }

//    DATABASE SERVICES
    public Movie findMovieInDbById(int id) {
        return movieDao.findById(id);
    }

    public boolean removeMovie(Movie movie) {
        return movieDao.remove(movie);
    }

    public List<Movie> getAllMovies() {
        return movieDao.getAll();
    }

    public Movie createOrUpdate(Movie movie) {
        return movieDao.createOrUpdate(movie);
    }
}
