/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.services;

import de.maymus.rest.TmdbService;
import de.maymus.dataaccess.MovieListDao;
import de.maymus.models.Movie;
import de.maymus.models.MovieList;
import de.maymus.models.User;
import static de.maymus.services.MovieService.LOG;
import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.model.Discover;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import java.util.LinkedList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class MovieListService extends TmdbService {

    static final Logger LOG = LoggerFactory.getLogger(MovieListService.class);

    private final transient TmdbMovies movies;
    private final transient TmdbApi api;
    final MovieListDao mlDao;

    public MovieListService() {
        LOG.info("* Start new MovieListApiService *");
        movies = getInstance().getTmdb().getMovies();
        api = getInstance().getTmdb();
        mlDao = new MovieListDao();
    }

    public MovieListService(Datastore ds) {
        LOG.info("* Start new MovieListApiService *");
        movies = getInstance().getTmdb().getMovies();
        api = getInstance().getTmdb();
        mlDao = new MovieListDao(ds);
    }

    public List<Movie> updateNowCinema(int page) {
        LOG.info("Get Now Playing Movies.");
        List<Movie> list = new LinkedList<>();
        movies.getNowPlayingMovies("de", page).forEach((t) -> {
            list.add(new Movie(t));
        });
        return list;
    }

    public List<Movie> updateUpcomming(int page) {
        LOG.info("Get Upcomming Movies.");
        List<Movie> list = new LinkedList<>();
        movies.getUpcoming("de", page).forEach((t) -> {
            list.add(new Movie(t));
        });
        return list;
    }

    public List<Movie> updateTopRated(int page) {
        LOG.info("Get Top Rated Movies.");
        Discover dsc = new Discover();
        dsc.language("de").page(page).sortBy("vote_average.desc").voteCountGte(1000);

        List<Movie> list = new LinkedList<>();
        api.getDiscover().getDiscover(dsc).getResults().forEach((t) -> {
            list.add(new Movie(t));
        });
        return list;
    }

    //Database
    public MovieList findMovieListByName(String name) {
        MovieList ret = mlDao.findMovieListByName(name);
        String log = ret != null ? "List Found!" : "List NOT Found!";
        LOG.info(log);
        return ret;
    }

    public boolean removeMovieList(MovieList ml) {
        boolean ret = mlDao.remove(ml);
        String log = ret ? "Successfully deleted!" : "NOT deleted!";
        LOG.info(log);
        return ret;
    }

    public MovieList createOrUpdateList(MovieList list) {
        MovieList ret = mlDao.createOrUpdateList(list);
        String log = ret != null ? "CreateOrUpdate Successfull!" : "CreateOrUpdate NOT Successfull!";
        LOG.info(log);
        return ret;
    }

}
