/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@Entity("movieList")
public class MovieList implements Serializable {

    static final Logger LOG = LoggerFactory.getLogger(MovieList.class);
    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    @Version
    private long mongo_version;
    private String desc;
    private boolean share;

    private LocalDateTime last_edit;

    @Reference
    private List<Movie> movies = new ArrayList<Movie>();

    public MovieList() {

    }

    public MovieList(List<Movie> mlist, String name, String desc, boolean publicity) {
        super();
        this.movies = mlist;
        this.name = name;
        this.last_edit = TimeHelper.getTime();
        this.desc = desc;
        this.share = publicity;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public User getOwner() {
        return null;
    }

    public void setOwner(User owner) {
    }

    public long getMongo_version() {
        return mongo_version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getLast_edit() {
        return last_edit;
    }

    public void setLast_edit(LocalDateTime last_edit) {
        this.last_edit = last_edit;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MovieList other = (MovieList) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (!Objects.equals(this.movies, other.movies)) {
            return false;
        }
        return true;
    }

    public boolean addMovie(MovieList list, Movie movie) {
        if (!list.getMovies().contains(movie)) {
            return list.getMovies().add(movie);
        }
        return false;
    }

    public boolean removeMovie(MovieList list, Movie movie) {
        if (list.getMovies().contains(movie)) {
            return list.getMovies().remove(movie);
        }
        return false;
    }

}
