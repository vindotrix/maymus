/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.Constants;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;

/**
 *
 * @author vindotrix
 */
@Entity("users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private ObjectId id;

    @Indexed(options = @IndexOptions(unique = true))
    private String username;

    @Indexed(options = @IndexOptions(unique = true))
    private String email;
    private String password;
    private Locale language;
    private LocalDateTime last_online;

    private MovieList movie_favorite;
    private MovieList movie_watchlist;
    private List<MovieList> movie_customLists;

    private SeriesList series_favorite;
    private SeriesList series_watchlist;
    private List<SeriesList> series_customLists;

    private GameList games_favorite;
    private GameList games_watchlist;
    private List<GameList> games_customLists;

    //MORPHIA CONSTRUCTOR
    public User() {
        super();
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.language = new Locale("de");
        this.movie_favorite = new MovieList(new LinkedList<>(), Constants.FAVORITE, "movie.list.user.favorite.desc", false);
        this.movie_watchlist = new MovieList(new LinkedList<>(), Constants.WATCH, "movie.list.user.watchlist.desc", false);
        this.movie_customLists = new LinkedList<>();
        this.series_favorite = new SeriesList(new LinkedList<>(), Constants.FAVORITE, "");
        this.series_watchlist = new SeriesList(new LinkedList<>(), Constants.WATCH, "");
        this.series_customLists = new LinkedList<>();
        this.games_favorite = new GameList(Constants.FAVORITE, "", false, new LinkedList<>());
        this.games_watchlist = new GameList(Constants.WATCH, "", false, new LinkedList<>());
        this.games_customLists = new LinkedList<>();

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getLast_online() {
        return last_online;
    }

    public void setLast_online(LocalDateTime last_online) {
        this.last_online = last_online;
    }

    public MovieList getMovieFavorite() {
        return movie_favorite;
    }

    public void setMovieFavorite(MovieList favorite) {
        this.movie_favorite = favorite;
    }

    public MovieList getMovieWatchlist() {
        return movie_watchlist;
    }

    public void setMovieWatchlist(MovieList watchlist) {
        this.movie_watchlist = watchlist;
    }

    public List<MovieList> getMovieCustomLists() {
        return movie_customLists;
    }

    public void setMovieCustomLists(List<MovieList> customLists) {
        this.movie_customLists = customLists;
    }

    public SeriesList getSeries_favorite() {
        return series_favorite;
    }

    public void setSeries_favorite(SeriesList series_favorite) {
        this.series_favorite = series_favorite;
    }

    public SeriesList getSeries_watchlist() {
        return series_watchlist;
    }

    public void setSeries_watchlist(SeriesList series_watchlist) {
        this.series_watchlist = series_watchlist;
    }

    public List<SeriesList> getSeries_customLists() {
        return series_customLists;
    }

    public void setSeries_customLists(List<SeriesList> series_customLists) {
        this.series_customLists = series_customLists;
    }

    public GameList getGames_favorite() {
        return games_favorite;
    }

    public void setGames_favorite(GameList games_favorite) {
        this.games_favorite = games_favorite;
    }

    public GameList getGames_watchlist() {
        return games_watchlist;
    }

    public void setGames_watchlist(GameList games_watchlist) {
        this.games_watchlist = games_watchlist;
    }

    public List<GameList> getGames_customLists() {
        return games_customLists;
    }

    public void setGames_customLists(List<GameList> games_customLists) {
        this.games_customLists = games_customLists;
    }

    public Locale getLanguage() {
        return language;
    }

    public void setLanguage(Locale language) {
        this.language = language;
    }

    public String shortUsername() {
        if (username != null && !username.isEmpty()) {
            if (username.length() > 4) {
                return username.substring(0, 2).concat("..");
            } else {
                return username;
            }
        }
        return username;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", email=" + email + ", password=" + password + ", last_online=" + last_online + '}';
    }

    public String toShortString() {
        return "User{" + "id=" + id + ", username=" + username + ", email=" + email + ", last_online: " + last_online + "}";
    }
}
