/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.model.tv.TvSeries;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@Entity("series")
public class Series implements Serializable {

    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(Series.class);

    @Id
    private int series_id;
    private String name;
    private String poster;
    private String story;
    private float rating;
    private String releaseDate;
    private String status;
    private LocalDateTime timestamp;
    private int seasons;
    private int episodes;

    //MORPHIA CONSTRUCTOR
    public Series() {
        super();
    }

    public Series(TvSeries series) {
        this.series_id = series.getId();
        this.name = series.getName();
        this.poster = series.getPosterPath();
        this.rating = series.getVoteAverage();
        this.releaseDate = series.getFirstAirDate();
        this.status = series.getStatus();
        this.timestamp = TimeHelper.getTime();
        this.seasons = series.getNumberOfSeasons();
        this.episodes = series.getNumberOfEpisodes();

        if (series.getOverview() == null || series.getOverview().length() == 0) {
            this.story = "Leider kein Inhalt verfügbar :(";
        } else if (series.getOverview().length() > 200) {
            String tmp = series.getOverview().substring(0, 200);
            this.story = tmp.substring(0, tmp.lastIndexOf(" ")).concat("...");
        } else {
            this.story = series.getOverview();
        }

    }

    public int getSeries_id() {
        return series_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getSeasons() {
        return seasons;
    }

    public void setSeasons(int seasons) {
        this.seasons = seasons;
    }

    public int getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int episodes) {
        this.episodes = episodes;
    }

    public String getRatingAsString() {
        String ret = Float.toString(rating).substring(0, 3);
        return ret.equals("0.0") ? "-" : ret;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.series_id;
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.poster);
        hash = 41 * hash + Objects.hashCode(this.story);
        hash = 41 * hash + Float.floatToIntBits(this.rating);
        hash = 41 * hash + Objects.hashCode(this.releaseDate);
        hash = 41 * hash + Objects.hashCode(this.status);
        hash = 41 * hash + Objects.hashCode(this.timestamp);
        hash = 41 * hash + this.seasons;
        hash = 41 * hash + this.episodes;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Series other = (Series) obj;
        if (this.series_id != other.series_id) {
            return false;
        }
        return true;
    }

}
