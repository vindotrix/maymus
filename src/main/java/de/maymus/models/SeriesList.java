/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Version;

/**
 *
 * @author Kevin
 */
@Entity("seriesList")
public class SeriesList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    @Version
    private long mongo_version;

    private String desc;
    private boolean share;
    private LocalDateTime last_edit;

    @Reference
    private List<Series> series = new ArrayList<Series>();

    public SeriesList() {

    }

    public SeriesList(List<Series> mlList, String name, String desc, boolean publicity) {
        super();
        this.series = mlList;
        this.name = name;
        this.last_edit = TimeHelper.getTime();
        this.desc = desc;
        this.share = publicity;
    }

    public SeriesList(List<Series> mlList, String name, String desc) {
        super();
        this.series = mlList;
        this.name = name;
        this.last_edit = TimeHelper.getTime();
        this.desc = desc;
        this.share = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String list_name) {
        this.name = list_name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public LocalDateTime getLast_edit() {
        return last_edit;
    }

    public void setLast_edit(LocalDateTime last_edit) {
        this.last_edit = last_edit;
    }

    public List<Series> getSeries() {
        return series;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.name);
        hash = 89 * hash + (int) (this.mongo_version ^ (this.mongo_version >>> 32));
        hash = 89 * hash + Objects.hashCode(this.desc);
        hash = 89 * hash + (this.share ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.last_edit);
        hash = 89 * hash + Objects.hashCode(this.series);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SeriesList other = (SeriesList) obj;
        return this.name.equals(other.name);
    }

    public boolean addSeries(SeriesList list, Series series) {
        if (!list.getSeries().contains(series)) {
            return list.getSeries().add(series);
        }
        return false;
    }

    public boolean removeSeries(SeriesList list, Series series) {
        if (list.getSeries().contains(series)) {
            return list.getSeries().remove(series);
        }
        return false;
    }

}
