/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.model.MovieDb;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
@Entity("movie")
public class Movie implements Serializable {

    static final Logger LOG = LoggerFactory.getLogger(Movie.class);
    private static final long serialVersionUID = 1L;

    @Id
    private int movie_id;

    private String name;
    private String poster;
    private String story;
    private float rating;
    private String releaseDate;
    private int runtime;
    private LocalDateTime timestamp;

    //MORPHIA CONSTRUCTOR
    public Movie() {
        super();
    }

    public Movie(MovieDb movie) {
        this.movie_id = movie.getId();
        this.name = movie.getTitle();
        this.poster = movie.getPosterPath();
        this.releaseDate = movie.getReleaseDate();
        this.rating = movie.getVoteAverage();
        this.runtime = movie.getRuntime();
        this.timestamp = TimeHelper.getTime();

        if (movie.getOverview() == null || movie.getOverview().length() == 0) {
            this.story = "Leider kein Inhalt verfügbar :(";
        } else if (movie.getOverview().length() > 240) {
            String tmp = movie.getOverview().substring(0, 240);
            this.story = tmp.substring(0, tmp.lastIndexOf(" ")).concat("...");
        } else {
            this.story = movie.getOverview();
        }
        System.out.println("");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public float getRating() {
        return rating;
    }

    public String getOptRating() {
        String ret = Float.toString(rating).substring(0, 3);

        return ret.equals("0.0") ? "-" : ret;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.movie_id;
        hash = 53 * hash + Objects.hashCode(this.name);
        hash = 53 * hash + Objects.hashCode(this.poster);
        hash = 53 * hash + Objects.hashCode(this.story);
        hash = 53 * hash + Float.floatToIntBits(this.rating);
        hash = 53 * hash + Objects.hashCode(this.releaseDate);
        hash = 53 * hash + this.runtime;
        hash = 53 * hash + Objects.hashCode(this.timestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movie other = (Movie) obj;
        if (this.movie_id != other.movie_id) {
            return false;
        }
        return true;
    }

}
