/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@Entity("gameList")
public class GameList implements Serializable {

    static final Logger LOG = LoggerFactory.getLogger(MovieList.class);
    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    @Version
    private long mongo_version;
    private String desc;
    private boolean share;

    private LocalDateTime last_edit;

    @Reference
    private List<Game> games = new ArrayList<Game>();

    public GameList() {

    }

    public GameList(String name, String desc, boolean share, List<Game> games) {
        this.name = name;
        this.desc = desc;
        this.share = share;
        this.last_edit = TimeHelper.getTime();
        this.games = games;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMongo_version() {
        return mongo_version;
    }

    public void setMongo_version(long mongo_version) {
        this.mongo_version = mongo_version;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public LocalDateTime getLast_edit() {
        return last_edit;
    }

    public void setLast_edit(LocalDateTime last_edit) {
        this.last_edit = last_edit;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public boolean addGame(GameList list, Game game) {
        if (!list.getGames().contains(game)) {
            return list.getGames().add(game);
        }
        return false;
    }

    public boolean removeGame(GameList list, Game game) {
        if (list.getGames().contains(game)) {
            return list.getGames().remove(game);
        }
        return false;
    }

}
