/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "summary",
    "franchise",
    "game_modes",
    "pegi",
    "keywords",
    "developers",
    "rating",
    "videos",
    "url",
    "time_to_beat",
    "player_perspectives",
    "rating_count",
    "cover",
    "external",
    "genres",
    "first_release_date",
    "release_dates",
    "name",
    "publishers",
    "game_engines",
    "websites",
    "id",
    "category",
    "expansions"
})
public class GameDetail {

    @JsonProperty("summary")
    private String summary;
    @JsonProperty("franchise")
    private Franchise franchise;
    @JsonProperty("game_modes")
    private List<GameMode> gameModes = null;
    @JsonProperty("pegi")
    private Pegi pegi;
    @JsonProperty("keywords")
    private List<Keyword> keywords = null;
    @JsonProperty("developers")
    private List<Developer> developers = null;
    @JsonProperty("rating")
    private Double rating;
    @JsonProperty("videos")
    private List<Video> videos = null;
    @JsonProperty("url")
    private String url;
    @JsonProperty("time_to_beat")
    private TimeToBeat timeToBeat;
    @JsonProperty("player_perspectives")
    private List<PlayerPerspective> playerPerspectives = null;
    @JsonProperty("rating_count")
    private Integer ratingCount;
    @JsonProperty("cover")
    private Cover cover;
    @JsonProperty("external")
    private External external;
    @JsonProperty("genres")
    private List<Genre> genres = null;
    @JsonProperty("first_release_date")
    private Long firstReleaseDate;
    @JsonProperty("release_dates")
    private List<ReleaseDate> releaseDates = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("publishers")
    private List<Publisher> publishers = null;
    @JsonProperty("game_engines")
    private List<GameEngine> gameEngines = null;
    @JsonProperty("websites")
    private List<Website> websites = null;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("category")
    private Integer category;
    @JsonProperty("expansions")
    private List<Expansion> expansions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("franchise")
    public Franchise getFranchise() {
        return franchise;
    }

    @JsonProperty("franchise")
    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    @JsonProperty("game_modes")
    public List<GameMode> getGameModes() {
        return gameModes;
    }

    @JsonProperty("game_modes")
    public void setGameModes(List<GameMode> gameModes) {
        this.gameModes = gameModes;
    }

    @JsonProperty("pegi")
    public Pegi getPegi() {
        return pegi;
    }

    @JsonProperty("pegi")
    public void setPegi(Pegi pegi) {
        this.pegi = pegi;
    }

    @JsonProperty("keywords")
    public List<Keyword> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("developers")
    public List<Developer> getDevelopers() {
        return developers;
    }

    @JsonProperty("developers")
    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    @JsonProperty("rating")
    public Double getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Double rating) {
        this.rating = rating;
    }

    @JsonProperty("videos")
    public List<Video> getVideos() {
        return videos;
    }

    @JsonProperty("videos")
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("time_to_beat")
    public TimeToBeat getTimeToBeat() {
        return timeToBeat;
    }

    @JsonProperty("time_to_beat")
    public void setTimeToBeat(TimeToBeat timeToBeat) {
        this.timeToBeat = timeToBeat;
    }

    @JsonProperty("player_perspectives")
    public List<PlayerPerspective> getPlayerPerspectives() {
        return playerPerspectives;
    }

    @JsonProperty("player_perspectives")
    public void setPlayerPerspectives(List<PlayerPerspective> playerPerspectives) {
        this.playerPerspectives = playerPerspectives;
    }

    @JsonProperty("rating_count")
    public Integer getRatingCount() {
        return ratingCount;
    }

    @JsonProperty("rating_count")
    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    @JsonProperty("cover")
    public Cover getCover() {
        return cover;
    }

    @JsonProperty("cover")
    public void setCover(Cover cover) {
        this.cover = cover;
    }

    @JsonProperty("external")
    public External getExternal() {
        return external;
    }

    @JsonProperty("external")
    public void setExternal(External external) {
        this.external = external;
    }

    @JsonProperty("genres")
    public List<Genre> getGenres() {
        return genres;
    }

    @JsonProperty("genres")
    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    @JsonProperty("first_release_date")
    public Long getFirstReleaseDate() {
        return firstReleaseDate;
    }

    @JsonProperty("first_release_date")
    public void setFirstReleaseDate(Long firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }

    @JsonProperty("release_dates")
    public List<ReleaseDate> getReleaseDates() {
        return releaseDates;
    }

    @JsonProperty("release_dates")
    public void setReleaseDates(List<ReleaseDate> releaseDates) {
        this.releaseDates = releaseDates;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("publishers")
    public List<Publisher> getPublishers() {
        return publishers;
    }

    @JsonProperty("publishers")
    public void setPublishers(List<Publisher> publishers) {
        this.publishers = publishers;
    }

    @JsonProperty("game_engines")
    public List<GameEngine> getGameEngines() {
        return gameEngines;
    }

    @JsonProperty("game_engines")
    public void setGameEngines(List<GameEngine> gameEngines) {
        this.gameEngines = gameEngines;
    }

    @JsonProperty("websites")
    public List<Website> getWebsites() {
        return websites;
    }

    @JsonProperty("websites")
    public void setWebsites(List<Website> websites) {
        this.websites = websites;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("category")
    public Integer getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Integer category) {
        this.category = category;
    }

    @JsonProperty("expansions")
    public List<Expansion> getExpansions() {
        return expansions;
    }

    @JsonProperty("expansions")
    public void setExpansions(List<Expansion> expansions) {
        this.expansions = expansions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
