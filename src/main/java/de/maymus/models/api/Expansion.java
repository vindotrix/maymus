/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "summary",
    "franchise",
    "game",
    "game_modes",
    "keywords",
    "developers",
    "rating",
    "videos",
    "url",
    "time_to_beat",
    "player_perspectives",
    "rating_count",
    "cover",
    "genres",
    "first_release_date",
    "release_dates",
    "name",
    "publishers",
    "websites",
    "id",
    "category",
    "game_engines"
})
public class Expansion {

    @JsonProperty("summary")
    private String summary;
    @JsonProperty("franchise")
    private Integer franchise;
    @JsonProperty("game")
    private Integer game;
    @JsonProperty("game_modes")
    private List<Integer> gameModes = null;
    @JsonProperty("keywords")
    private List<Integer> keywords = null;
    @JsonProperty("developers")
    private List<Integer> developers = null;
    @JsonProperty("rating")
    private Double rating;
    @JsonProperty("videos")
    private List<Video_> videos = null;
    @JsonProperty("url")
    private String url;
    @JsonProperty("time_to_beat")
    private TimeToBeat_ timeToBeat;
    @JsonProperty("player_perspectives")
    private List<Integer> playerPerspectives = null;
    @JsonProperty("rating_count")
    private Integer ratingCount;
    @JsonProperty("cover")
    private Cover_ cover;
    @JsonProperty("genres")
    private List<Integer> genres = null;
    @JsonProperty("first_release_date")
    private Long firstReleaseDate;
    @JsonProperty("release_dates")
    private List<ReleaseDate_> releaseDates = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("publishers")
    private List<Integer> publishers = null;
    @JsonProperty("websites")
    private List<Website_> websites = null;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("category")
    private Integer category;
    @JsonProperty("game_engines")
    private List<Integer> gameEngines = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("franchise")
    public Integer getFranchise() {
        return franchise;
    }

    @JsonProperty("franchise")
    public void setFranchise(Integer franchise) {
        this.franchise = franchise;
    }

    @JsonProperty("game")
    public Integer getGame() {
        return game;
    }

    @JsonProperty("game")
    public void setGame(Integer game) {
        this.game = game;
    }

    @JsonProperty("game_modes")
    public List<Integer> getGameModes() {
        return gameModes;
    }

    @JsonProperty("game_modes")
    public void setGameModes(List<Integer> gameModes) {
        this.gameModes = gameModes;
    }

    @JsonProperty("keywords")
    public List<Integer> getKeywords() {
        return keywords;
    }

    @JsonProperty("keywords")
    public void setKeywords(List<Integer> keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("developers")
    public List<Integer> getDevelopers() {
        return developers;
    }

    @JsonProperty("developers")
    public void setDevelopers(List<Integer> developers) {
        this.developers = developers;
    }

    @JsonProperty("rating")
    public Double getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Double rating) {
        this.rating = rating;
    }

    @JsonProperty("videos")
    public List<Video_> getVideos() {
        return videos;
    }

    @JsonProperty("videos")
    public void setVideos(List<Video_> videos) {
        this.videos = videos;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("time_to_beat")
    public TimeToBeat_ getTimeToBeat() {
        return timeToBeat;
    }

    @JsonProperty("time_to_beat")
    public void setTimeToBeat(TimeToBeat_ timeToBeat) {
        this.timeToBeat = timeToBeat;
    }

    @JsonProperty("player_perspectives")
    public List<Integer> getPlayerPerspectives() {
        return playerPerspectives;
    }

    @JsonProperty("player_perspectives")
    public void setPlayerPerspectives(List<Integer> playerPerspectives) {
        this.playerPerspectives = playerPerspectives;
    }

    @JsonProperty("rating_count")
    public Integer getRatingCount() {
        return ratingCount;
    }

    @JsonProperty("rating_count")
    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    @JsonProperty("cover")
    public Cover_ getCover() {
        return cover;
    }

    @JsonProperty("cover")
    public void setCover(Cover_ cover) {
        this.cover = cover;
    }

    @JsonProperty("genres")
    public List<Integer> getGenres() {
        return genres;
    }

    @JsonProperty("genres")
    public void setGenres(List<Integer> genres) {
        this.genres = genres;
    }

    @JsonProperty("first_release_date")
    public Long getFirstReleaseDate() {
        return firstReleaseDate;
    }

    @JsonProperty("first_release_date")
    public void setFirstReleaseDate(Long firstReleaseDate) {
        this.firstReleaseDate = firstReleaseDate;
    }

    @JsonProperty("release_dates")
    public List<ReleaseDate_> getReleaseDates() {
        return releaseDates;
    }

    @JsonProperty("release_dates")
    public void setReleaseDates(List<ReleaseDate_> releaseDates) {
        this.releaseDates = releaseDates;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("publishers")
    public List<Integer> getPublishers() {
        return publishers;
    }

    @JsonProperty("publishers")
    public void setPublishers(List<Integer> publishers) {
        this.publishers = publishers;
    }

    @JsonProperty("websites")
    public List<Website_> getWebsites() {
        return websites;
    }

    @JsonProperty("websites")
    public void setWebsites(List<Website_> websites) {
        this.websites = websites;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("category")
    public Integer getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Integer category) {
        this.category = category;
    }

    @JsonProperty("game_engines")
    public List<Integer> getGameEngines() {
        return gameEngines;
    }

    @JsonProperty("game_engines")
    public void setGameEngines(List<Integer> gameEngines) {
        this.gameEngines = gameEngines;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
