/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models.api;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "y",
    "category",
    "region",
    "human",
    "m",
    "platform"
})
public class ReleaseDate_ {

    @JsonProperty("date")
    private long date;
    @JsonProperty("y")
    private Integer y;
    @JsonProperty("category")
    private Integer category;
    @JsonProperty("region")
    private Integer region;
    @JsonProperty("human")
    private String human;
    @JsonProperty("m")
    private Integer m;
    @JsonProperty("platform")
    private Integer platform;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("date")
    public long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(long date) {
        this.date = date;
    }

    @JsonProperty("y")
    public Integer getY() {
        return y;
    }

    @JsonProperty("y")
    public void setY(Integer y) {
        this.y = y;
    }

    @JsonProperty("category")
    public Integer getCategory() {
        return category;
    }

    @JsonProperty("category")
    public void setCategory(Integer category) {
        this.category = category;
    }

    @JsonProperty("region")
    public Integer getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(Integer region) {
        this.region = region;
    }

    @JsonProperty("human")
    public String getHuman() {
        return human;
    }

    @JsonProperty("human")
    public void setHuman(String human) {
        this.human = human;
    }

    @JsonProperty("m")
    public Integer getM() {
        return m;
    }

    @JsonProperty("m")
    public void setM(Integer m) {
        this.m = m;
    }

    @JsonProperty("platform")
    public Integer getPlatform() {
        return platform;
    }

    @JsonProperty("platform")
    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
