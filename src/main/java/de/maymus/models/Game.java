/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.models;

import de.maymus.models.api.GameDetail;
import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.logging.Level;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@Entity("game")
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;
    static final Logger LOG = LoggerFactory.getLogger(Game.class);

    @Id
    private int game_id;

    private String name;
    private String poster;
    private String story;
    private Double rating;
    private String releaseDate;
    private LocalDateTime timestamp;

    public Game() {
        super();
    }

    public Game(GameDetail data) {
        this.game_id = data.getId();
        this.name = data.getName() != null ? data.getName() : "-";
        this.poster = data.getCover() != null && data.getCover().getCloudinaryId() != null ? data.getCover().getCloudinaryId() : "";
        this.story = data.getSummary() != null ? data.getSummary() : "-";
        this.rating = data.getRating() != null ? getRating(data.getRating()) : 00.0;
        this.releaseDate = data.getFirstReleaseDate() != null ? TimeHelper.getDateFromTimestamp(data.getFirstReleaseDate()) : "-";
        this.timestamp = TimeHelper.getTime();
    }

    private Double getRating(double input) {
        try {
            DecimalFormat df = new DecimalFormat("#.0");
            return NumberFormat.getInstance().parse(df.format(input)).doubleValue();
        } catch (ParseException ex) {
            LOG.error("Error by rounding a double:" + input);
        }
        return 00.0;
    }

    public int getGame_id() {
        return game_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.game_id;
        hash = 19 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Game other = (Game) obj;
        if (this.game_id != other.game_id) {
            return false;
        }
        return true;
    }

}
