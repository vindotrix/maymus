/*
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.beans.BaseBean;
import de.maymus.models.Game;
import de.maymus.models.Movie;
import de.maymus.models.Series;
import de.maymus.services.GameListService;
import de.maymus.services.MovieListService;
import de.maymus.services.SeriesListService;
import de.maymus.util.Constants;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author brach.kevin@stud.htwk-leipzig.de
 */
@Named
@ViewScoped
public class WelcomeBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private MovieListService ms;
    private SeriesListService ss;
    private GameListService gs;
    private List<Game> games;
    private List<Movie> movies;
    private List<Series> series;

    @PostConstruct
    private void init() {
        ms = new MovieListService();
        ss = new SeriesListService();
        gs = new GameListService();
        requestGames();
    }

    private void requestGames() {
        List<Movie> tmp1 = ms.findMovieListByName(Constants.TOP_RATED).getMovies();
        List<Game> tmp2 = gs.findGameListByName(Constants.TOP_RATED).getGames();
        List<Series> tmp3 = ss.findSeriesListByName(Constants.TOP_RATED).getSeries();

        movies = tmp1.size() > 6 ? tmp1.subList(0, 6) : tmp1;
        series = tmp3.size() > 6 ? tmp3.subList(0, 6) : tmp3;
        games = tmp2.size() > 6 ? tmp2.subList(0, 6) : tmp2;
    }

    /**
     *
     * @return
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     *
     * @param games
     */
    public void setGames(List<Game> games) {
        this.games = games;
    }

    /**
     *
     * @return
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     *
     * @param movies
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     *
     * @return
     */
    public List<Series> getSeries() {
        return series;
    }

    /**
     *
     * @param series
     */
    public void setSeries(List<Series> series) {
        this.series = series;
    }

}
