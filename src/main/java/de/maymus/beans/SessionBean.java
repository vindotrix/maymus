/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.User;
import de.maymus.services.GameService;
import de.maymus.services.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author vindotrix
 */
@SessionScoped
@Named
public class SessionBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private final transient UserService userService;
    private User loggedUser;
    private Locale locale;
    private String search;

    /**
     *
     */
    public SessionBean() {
        log.info("..... Starting <SessionBean> .....");
        userService = new UserService();
    }

    /**
     *
     * @return
     */
    public boolean isLoggedIn() {
        return loggedUser != null;
    }

    /**
     *
     * @return
     */
    public String isLoggedInForwardHome() {
        //log.info("Check if User is loggedIn -> redirect: " + isLoggedIn());
        if (isLoggedIn()) {
            addWarnMessage(null, "error.404.userarea", null);
            return "/home?faces-redirect=true";
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String isNotLoggedInForwardMovie() {
        //log.info("Check if User is loggedIn -> redirect: " + isLoggedIn());
        if (!isLoggedIn()) {
            addWarnMessage(null, "error.404.userarea", null);
            return "/home?faces-redirect=true";
        }
        return null;
    }

    /**
     *
     * @param loggedUser
     */
    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    /**
     *
     * @return
     */
    public User getLoggedUser() {
        return loggedUser;
    }

    /**
     *
     * @return
     */
    public Locale getLocale() {
        if (loggedUser != null && loggedUser.getLanguage() != null) {
            locale = loggedUser.getLanguage();
        }
        if (locale == null) {
            locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        }
        return locale;
    }

    /**
     *
     * @return
     */
    public String getLanguage() {
        return getLocale().getLanguage();
    }

    /**
     *
     * @param language
     * @throws IOException
     */
    public void changeLanguage(String language) throws IOException {
        locale = new Locale(language);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);

        if (loggedUser != null) {
            loggedUser.setLanguage(locale);
            userService.updateUser(loggedUser);
        }

        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String contextPath = origRequest.getContextPath();
        try {
            FacesContext.getCurrentInstance().getExternalContext()
                    .redirect(contextPath + "/");
        } catch (IOException e) {
        }

    }

    /**
     *
     * @return
     */
    public String getSearch() {
        return search;
    }

    /**
     *
     * @param search
     */
    public void setSearch(String search) {
        this.search = search;
        invokeSearch();
    }

    /**
     *
     */
    public void invokeSearch() {
        String path = "/maymus/pages/search.xhtml?search=" + search;
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        search = "";
        try {
            ectx.redirect(path);
        } catch (IOException ex) {
            log.info("Error while redirect to search.xhtml");
        }
    }

}
