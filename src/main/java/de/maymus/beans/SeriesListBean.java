/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Series;
import de.maymus.models.SeriesList;
import de.maymus.models.User;
import de.maymus.services.SeriesListService;
import de.maymus.services.SeriesService;
import de.maymus.services.UserService;
import de.maymus.util.Constants;
import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class SeriesListBean extends BaseBean implements Serializable, IListBean {

    private static final long serialVersionUID = 1L;

    private UserService userService;
    private SeriesService seriesService;
    private SeriesListService seriesListService;

    @Inject
    private transient SessionBean sessionBean;

    private SeriesList seriesList;
    private String paramsType;
    private String paramsList;
    private String paramsUser;
    private User loggedUser = null;
    private String listname;
    private String editName;
    private String editDesc;
    private boolean editPublicity;
    private boolean userList = false;
    private boolean publicList = false;

    /**
     *
     */
    @PostConstruct
    public void initBean() {
        log.info("..... Starting <SeriesListBean> .....");
        userService = new UserService();
        seriesService = new SeriesService();
        seriesListService = new SeriesListService();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            paramsList = params.get("list");
            paramsType = params.get("type");
            paramsUser = params.get("user");
        } catch (Exception e) {
            System.out.println("Params not found.");
        }

        if (sessionBean != null && sessionBean.getLoggedUser() != null) {
            this.loggedUser = sessionBean.getLoggedUser();
        }

    }

    private boolean isLogged() {
        if (sessionBean.isLoggedIn()) {
            loggedUser = sessionBean.getLoggedUser();
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @return
     */
    public String requestSeriesList() {
        if (paramsType != null && paramsType.length() > 0 && paramsList != null && paramsList.length() > 0) {
            switch (paramsType) {
                case Constants.LIST_TYPE_MAYMUS:
                    userList = false;
                    return requestMaymusList();
                case Constants.LIST_TYPE_USER:
                    if (isLogged()) {
                        userList = true;
                        return requestUserList();
                    } else {
                        return redirectToHomeNotLoggedIn();
                    }
            }
        }
        return redirect404();
    }

    private String requestMaymusList() {
        if (paramsList.equals(Constants.TOP_RATED) || paramsList.equals(Constants.UPCOMMING) || paramsList.equals(Constants.NOW_IN_TV)) {
            seriesList = seriesListService.findSeriesListByName(paramsList);
            listname = getMsgFromBundle(Constants.seriesListName(paramsList));
            if (seriesList != null) {
                return "";
            }
        }
        return redirect404();

    }

    private String requestUserList() {
        switch (paramsList) {
            case Constants.FAVORITE:
                seriesList = loggedUser.getSeries_favorite();
                break;
            case Constants.WATCH:
                seriesList = loggedUser.getSeries_watchlist();
                break;
        }
        if (seriesList != null) {
            listname = getMsgFromBundle(Constants.userListName(paramsList));
            return "";
        }
        return redirect404();
    }

    /**
     *
     * @return
     */
    public String requestUserCustomList() {
        if (isLogged()) {

            for (SeriesList sl : loggedUser.getSeries_customLists()) {
                if (paramsList.equals(sl.getName())) {
                    seriesList = sl;
                    listname = sl.getName();
                    editName = listname;
                    editDesc = sl.getDesc();
                    editPublicity = sl.isShare();
                }
            }
            if (seriesList != null) {
                return "";
            }
            return redirect404();
        } else if (paramsUser != null) {
            for (SeriesList list : userService.findByUsername(paramsUser).getSeries_customLists()) {
                if (list.getName().equals(paramsList) && list.isShare()) {
                    seriesList = list;
                    listname = paramsUser + "' Liste: " + list.getName();
                    publicList = true;
                    return "";
                }
            }
        }
        return redirectToHomeNotLoggedIn();

    }

    /**
     *
     * @return
     */
    public SeriesList getSeriesList() {
        return seriesList;
    }

    /**
     *
     * @param seriesList
     */
    public void setSeriesList(SeriesList seriesList) {
        this.seriesList = seriesList;
    }

    @Override
    public String getListname() {
        return listname;
    }

    @Override
    public void setListname(String listname) {
        this.listname = listname;
    }

    @Override
    public String getEditName() {
        return editName;
    }

    @Override
    public void setEditName(String editName) {
        this.editName = editName;
    }

    @Override
    public String getEditDesc() {
        return editDesc;
    }

    @Override
    public void setEditDesc(String editDesc) {
        this.editDesc = editDesc;
    }

    @Override
    public boolean isEditPublicity() {
        return editPublicity;
    }

    @Override
    public void setEditPublicity(boolean editPublicity) {
        this.editPublicity = editPublicity;
    }

    /**
     *
     * @return
     */
    public String lastEdit() {
        return TimeHelper.prettyDateAndTime(seriesList.getLast_edit());
    }

    /**
     *
     * @return
     */
    public boolean isUserList() {
        return userList;
    }

    /**
     *
     * @param userList
     */
    public void setUserList(boolean userList) {
        this.userList = userList;
    }

    /**
     *
     * @return
     */
    public boolean isPublicList() {
        return publicList;
    }

    /**
     *
     * @param publicList
     */
    public void setPublicList(boolean publicList) {
        this.publicList = publicList;
    }

    @Override
    public String editList() {
        if (isLogged() && seriesList != null) {
            seriesList.setDesc(editDesc);
            seriesList.setName(editName);
            seriesList.setShare(editPublicity);
            loggedUser = userService.updateUser(loggedUser);
            listname = editName;

            addInfoMessage(null, "customList.edit.success", null);
            setMessageKeep();
            String x = "/maymus/pages/series/custom_list.xhtml?list=" + editName;
            return x;
        }
        addErrorMessage(null, "customList.edit.fail", null);
        return "/pages/series/custom_list.xhtml?faces-redirect=true";
    }

    @Override
    public String removeList() {
        if (isLogged() && seriesList != null) {
            if (loggedUser.getSeries_customLists().remove(seriesList)) {
                loggedUser = userService.updateUser(loggedUser);
                sessionBean.setLoggedUser(loggedUser);
                addInfoMessage(null, "customList.remove.success", null);
                setMessageKeep();
                return "/pages/series/custom.xhtml?faces-redirect=true";
            }
        }
        addErrorMessage(null, "customList.remove.fail", null);
        return "/pages/series/custom.xhtml?faces-redirect=true";
    }

    @Override
    public void removeFromList(int id) {
        if (isLogged()) {
            for (Series tv : seriesList.getSeries()) {
                if (tv.getSeries_id() == id) {
                    if (seriesList.removeSeries(seriesList, tv)) {
                        loggedUser = userService.updateUser(loggedUser);
                        sessionBean.setLoggedUser(loggedUser);
                        addInfoMessage(null, "series.customList.remove", null);
                    } else {
                        addWarnMessage(null, "error.again", null);
                    }
                    break;
                }
            }
        }
    }

}
