/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import com.mongodb.DuplicateKeyException;
import de.maymus.models.User;
import de.maymus.services.UserService;
import de.maymus.util.RandomString;
import de.maymus.util.SendMail;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Kevin
 */
@Named
@RequestScoped
public class LoginBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient SessionBean sessionBean;

    private transient UserService userService;
    private transient SendMail sendMail;
    private String username;
    private String email;
    private String password;
    private String password_change;

    @PostConstruct
    public void init() {
        userService = new UserService();
    }

    /**
     * Try to login
     *
     * @return
     */
    public String login() {
        log.info("User is logging in: " + getEmail());
        User loggedUser = userService.login(email, password);
        if (loggedUser != null && sessionBean != null) {
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "login.successfull", null);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

            return "/home?faces-redirect=true";
        } else {
            addErrorMessage(null, "login.failed", "login_button");
            password = "";
            return null;
        }
    }

    /**
     * Create an user account
     *
     * @return
     */
    public String signUp() {
        log.info("Signup new User: " + getUsername());
        boolean success = false;
        try {
            success = userService.signUp(username, email, password);
            if (success) {
                sendMail = new SendMail();
                sendMail.sendWelcomeMail(username, email);
                addInfoMessage(null, "login.register.successfull", null);
                setMessageKeep();
            } else {
                addErrorMessage(null, "login.register.error", "signup_button");
            }
        } catch (DuplicateKeyException e) {
            addWarnMessage(null, "login.register.error", "signup_button");
        } catch (Exception e) {
            addErrorMessage(null, "error", "signup_button");
        }

        clear();
        return success ? "/pages/login?faces-redirect=true" : null;

    }

    /**
     * Change user password
     *
     * @return
     */
    public String changePassword() {
        User user = sessionBean.getLoggedUser();
        boolean old = false;
        old = userService.verifyPassword(password, user.getPassword());
        if (old) {
            User user2 = userService.changePassword(password_change, user);
            if (user2 != null) {
                sessionBean.setLoggedUser(user2);
                addInfoMessage("", "user.change.password.success", "");
                setMessageKeep();
                password = "";
                password_change = "";
                sendMail = new SendMail();
                sendMail.sendSuccessfullChangedPassword(user2.getUsername(), user2.getEmail());
                return redirectHome();
            } else {
                addErrorMessage("", "error.again", "change_button");
            }
        } else {
            addErrorMessage("", "user.change.password.err.curr", "input_current");
        }
        password = "";
        password_change = "";
        return "";
    }

    /**
     * User logout
     *
     * @return
     */
    public String logout() {
        log.info("User is logging out!");
        clear();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        addInfoMessage(null, "logout.successfull", null);
        return "/home?faces-redirect=true";
    }

    /**
     * clear input fields
     */
    public void clear() {
        log.info("Clear input!");
        setUsername("");
        setPassword("");
        setEmail("");
    }

    /**
     * get new password
     *
     * @return
     */
    public String requestNewPassword() {
        RandomString gs = new RandomString();
        String str = gs.generateRandomString();
        User user = userService.changePassword(str, email);
        if (user != null) {
            sendMail = new SendMail();
            sendMail.sendRequestNewPassword(user.getUsername(), user.getEmail(), str);
        }
        addInfoMessage("", "user.change.password.request", "");
        setMessageKeep();
        return redirectHome();
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public String getPassword_change() {
        return password_change;
    }

    /**
     *
     * @param password_change
     */
    public void setPassword_change(String password_change) {
        this.password_change = password_change;
    }

}
