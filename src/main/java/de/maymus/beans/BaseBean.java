/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import static de.maymus.rest.TmdbService.getInstance;
import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.Utils;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class BaseBean implements Serializable {

    /**
     * Logger
     */
    protected final transient Logger log = LoggerFactory.getLogger(this.getClass());

    private static final long serialVersionUID = 1L;

    /**
     * Path to dummy image
     */
    protected final String POSTERPATH_DEFAULT = "/resources/images/dummy.png";

    /**
     * path to igdb poster
     */
    protected final String IGDB_POSTERPATH = "https://images.igdb.com/igdb/image/upload/t_";

    /**
     * path to youtube links
     */
    protected final String YOUTUBE = "https://www.youtube.com/embed/";

    /**
     * Return error path
     *
     * @return
     */
    public String redirect404() {
        return "/WEB-INF/errorpages/404.xhtml?faces-redirect=true";
    }

    /**
     * Return url to home
     *
     * @return
     */
    public String redirectHome() {
        return "/home?faces-redirect=true";
    }

    /**
     * return url to home
     *
     * @return
     */
    protected String redirectToHomeNotLoggedIn() {
        return "/home?faces-redirect=true";
    }

    /**
     * Generate TMDb Image Small
     *
     * @param url
     * @return
     */
    public String posterImageSM(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "w154").toString() : POSTERPATH_DEFAULT;
    }

    /**
     * Generate TMDb Image Med
     *
     * @param url
     * @return
     */
    public String posterImageMD(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "w342").toString() : POSTERPATH_DEFAULT;
    }

    /**
     * Generate TMDb Image Large
     *
     * @param url
     * @return
     */
    public String posterImageL(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "w500").toString() : POSTERPATH_DEFAULT;
    }

    /**
     * Generate TMDb Image #XLarge
     *
     * @param url
     * @return
     */
    public String posterImageXL(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "w780").toString() : POSTERPATH_DEFAULT;
    }

    /**
     * Generate TMDb Image Original
     *
     * @param url
     * @return
     */
    public String posterImageOriginal(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "original").toString() : POSTERPATH_DEFAULT;
    }

    /**
     * Generate TMDb BackgroundImage XL
     *
     * @param url
     * @return
     */
    public String backdropImageXL(String url) {
        return validateUrl(url) ? Utils.createImageUrl(getInstance().getTmdb(), url, "w1280").toString() : "ss";
    }

    /**
     * Generate Game poster small
     *
     * @param url
     * @return
     */
    public String posterGameSM(String url) {
        return url.length() > 0 ? IGDB_POSTERPATH + "720p" + "/" + url + ".jpg" : POSTERPATH_DEFAULT;
    }

    /**
     * generate game poster large
     *
     * @param url
     * @return
     */
    public String posterGameLG(String url) {
        return url.length() > 0 ? IGDB_POSTERPATH + "1080p" + "/" + url + ".jpg" : POSTERPATH_DEFAULT;
    }

    /**
     * Check if string is not null and length >1
     *
     * @param url
     * @return
     */
    protected boolean validateUrl(String url) {
        return (url != null && url.length() > 1);
    }

    /**
     * OLD
     *
     * @param summary
     * @param detail
     * @param severity
     * @param component
     */
    protected static void addContextMessageKeep(String summary, String detail, FacesMessage.Severity severity, String component) {
        FacesContext.getCurrentInstance().addMessage(component, new FacesMessage(severity, summary, detail));
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        RequestContext.getCurrentInstance().update("globalmessages");
    }

    /**
     * Keep message through page refresh
     */
    protected void setMessageKeep() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        RequestContext.getCurrentInstance().update("globalmessages");
    }

    /**
     * Adds an InfoMessage
     *
     * @param summary
     * @param resourceMessageKey
     * @param clientId
     */
    protected void addInfoMessage(String summary, final String resourceMessageKey, final String clientId) {
        FacesMessage facesMessage;
        final FacesContext facesContext = FacesContext.getCurrentInstance();

        if (StringUtils.isEmpty(summary) && !StringUtils.isEmpty(resourceMessageKey)) {
            final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext,
                    "msg");
            summary = bundle.getString(resourceMessageKey);
        }
        facesMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, "");
        facesContext.addMessage(clientId, facesMessage);
    }

    /**
     * Adds warn message
     *
     * @param summary
     * @param resourceMessageKey
     * @param clientId
     */
    protected void addWarnMessage(String summary, final String resourceMessageKey, final String clientId) {
        FacesMessage facesMessage;
        final FacesContext facesContext = FacesContext.getCurrentInstance();

        if (StringUtils.isEmpty(summary) && !StringUtils.isEmpty(resourceMessageKey)) {
            final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext,
                    "msg");
            summary = bundle.getString(resourceMessageKey);
        }
        facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, summary, "");
        facesContext.addMessage(clientId, facesMessage);
    }

    /**
     * adds error message
     *
     * @param summary
     * @param resourceMessageKey
     * @param clientId
     */
    protected void addErrorMessage(String summary, final String resourceMessageKey, final String clientId) {
        FacesMessage facesMessage;
        final FacesContext facesContext = FacesContext.getCurrentInstance();

        if (StringUtils.isEmpty(summary) && !StringUtils.isEmpty(resourceMessageKey)) {
            final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext,
                    "msg");
            summary = bundle.getString(resourceMessageKey);
        }
        facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, "");
        facesContext.addMessage(clientId, facesMessage);
    }

    /**
     * Gets an message from i18n
     *
     * @param key
     * @return
     */
    protected String getMsgFromBundle(String key) {
        final FacesContext facesContext = FacesContext.getCurrentInstance();
        final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
        try {
            String summary = bundle.getString(key);
            return summary != null ? summary : "Bundlekey not found";
        } catch (Exception e) {
            log.error("MSG Key not found: " + key);
            return "404";
        }
    }

    /**
     * Convert Time to DE format
     *
     * @param date
     * @return
     */
    public String converteTimeToDE(String date) {
        return TimeHelper.convertUsToDe(date);
    }

    /**
     * Convert Timestamp to date
     *
     * @param input
     * @return
     */
    public String converteTimestampToDate(Long input) {
        return TimeHelper.getDateFromTimestamp(input);
    }

    /**
     * Convert US date to DE date
     *
     * @param date
     * @return
     */
    public String convertUsGameToDe(String date) {
        return TimeHelper.convertUsGameToDe(date);
    }

    /**
     * Convert Tiemstamp to time
     *
     * @param input
     * @return
     */
    public String converteTimestampToTime(int input) {
        return TimeHelper.getTimeFromTimestamp(input);
    }

    /**
     * Make Date pretty
     *
     * @param inp
     * @return
     */
    public String getLastEdit(LocalDateTime inp) {
        return TimeHelper.prettyDateAndTime(inp);
    }
}
