/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Game;
import de.maymus.models.Movie;
import de.maymus.models.Series;
import de.maymus.services.GameService;
import de.maymus.services.MovieService;
import de.maymus.services.SeriesService;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class SearchBean extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private transient SeriesService seriesService;
    private transient GameService gameService;
    private transient MovieService movieService;

    @Inject
    private transient SessionBean sessionBean;

    private List<Game> games;
    private List<Movie> movies;
    private List<Series> series;
    private String key;

    @PostConstruct
    public void init() {
        log.info("..... Starting <SearchBean> .....");
        seriesService = new SeriesService();
        gameService = new GameService();
        movieService = new MovieService();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            key = params.get("search");
        } catch (Exception e) {
            System.out.println("");
        }

    }

    /**
     * Invokes a search
     *
     * @return
     */
    public String invokeSearch() {
        if (key != null && !key.equals("")) {
            try {
                games = gameService.searchGame(key);
                movies = movieService.searchMovies(key);
                series = seriesService.searchTv(key);
            } catch (Exception e) {
                log.error("Error while searching: " + e);
            }
            return "";
        } else {
            return redirect404();
        }
    }

    /**
     *
     * @return
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     *
     * @param games
     */
    public void setGames(List<Game> games) {
        this.games = games;
    }

    /**
     *
     * @return
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     *
     * @param movies
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    /**
     *
     * @return
     */
    public List<Series> getSeries() {
        return series;
    }

    /**
     *
     * @param series
     */
    public void setSeries(List<Series> series) {
        this.series = series;
    }

    /**
     *
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

}
