/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.GameList;

/**
 *
 * @author Kevin
 * @param <T>
 */
public interface IDetailBean<T> {

    /**
     * Add Item to a List
     *
     * @param name
     */
    void addToCustomlist(String name);

    /**
     * Add item to a Favorite list
     */
    void addToFavoriteList();

    /**
     * Add Item to a Watchlist
     */
    void addToWatchList();

    /**
     * Check if list is in given list
     *
     * @param list
     * @return
     */
    boolean onCustomlist(T list);

    /**
     * check if detail item is in fav. list
     *
     * @return
     */
    boolean onFavoritelist();

    /**
     * check if detail item is in watchlist
     *
     * @return
     */
    boolean onWatchlist();

    /**
     * Remove Item from given List
     *
     * @param name
     */
    void removeFromCustomlist(String name);

    /**
     * Remove detail Item from fav list
     */
    void removeFromFavoriteList();

    /**
     * Remove detail item from watchlist
     */
    void removeFromWatchList();

    /**
     * Create a new Customlist
     */
    void createNewCustomList();

    /**
     * Description for new Customlist
     *
     * @return
     */
    String getCustomListDesc();

    /**
     * Name for new Customlist
     *
     * @return
     */
    String getCustomListName();

    /**
     * Share boolean for new Customlist
     *
     * @return
     */
    boolean isCustomListPublicity();

    /**
     * Description for new Customlist
     *
     * @param customListDesc
     */
    void setCustomListDesc(String customListDesc);

    /**
     * Name for new Customlist
     *
     * @param customListName
     */
    void setCustomListName(String customListName);

    /**
     * Share boolean for new Customlist
     *
     * @param customListPublicity
     */
    void setCustomListPublicity(boolean customListPublicity);

}
