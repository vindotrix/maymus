/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Movie;
import de.maymus.models.MovieList;
import de.maymus.models.User;
import de.maymus.services.MovieService;
import de.maymus.services.TmdbDetailService;
import de.maymus.services.UserService;
import info.movito.themoviedbapi.model.Credits;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.Video;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.model.tagcloud.DefaultTagCloudItem;
import org.primefaces.model.tagcloud.DefaultTagCloudModel;
import org.primefaces.model.tagcloud.TagCloudModel;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class MovieDetailBean extends BaseBean implements Serializable, IDetailBean<MovieList> {

    private static final long serialVersionUID = 1L;

    private int id;
    private MovieDb movieDB;
    private Movie movie;
    private final transient MovieService movieApiService;
    private final transient TmdbDetailService detailService;
    private final transient UserService userService;
    private TagCloudModel tagModel;
    private Credits credits;
    private User loggedUser;
    private List<String> youtubeLinks = new LinkedList<>();
    private List<Movie> similar = new LinkedList<>();
    private String customListName;
    private String customListDesc;
    private boolean customListPublicity = false;

    /**
     * Constructor
     */
    public MovieDetailBean() {
        log.info("..... Starting <movieDetailBean> .....");
        movieApiService = new MovieService();
        detailService = new TmdbDetailService();
        userService = new UserService();
    }

    /**
     * Initialize Bean
     *
     * @param user
     * @param language
     */
    public void init(User user, String language) {
        log.info("Initialize Detailview!");
        loggedUser = user;
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        try {
            id = Integer.valueOf(params.get("id"));
            this.movieDB = movieApiService.getMovieById(id, language);
            this.movie = movieApiService.createOrUpdate(new Movie(movieDB));
        } catch (NumberFormatException o) {
            log.error("Movie id cannot be transformed to an int: " + params.get("id"));
        }
        if (movieDB != null) {
            credits = movieDB.getCredits();
            detailService.getSimilar(id).forEach((item) -> {
                similar.add(new Movie(item));
            });
            generateYoutubeLink();
            if (movieDB.getKeywords() != null && movieDB.getKeywords().size() > 0) {
                tagModel = new DefaultTagCloudModel();
                movieDB.getKeywords().forEach((item) -> {
                    tagModel.addTag(new DefaultTagCloudItem(item.getName(), 1 + (int) (Math.random() * 5)));
                });
            }
        } else {
            log.info("No Movie found by Id: " + params.get("id"));
            addErrorMessage(null, "error.404.movie", null);
            setMessageKeep();

            HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String contextPath = origRequest.getContextPath();
            try {
                FacesContext.getCurrentInstance().getExternalContext()
                        .redirect(contextPath + "/");
            } catch (IOException e) {
            }
        }
    }

    private void generateYoutubeLink() {
        List<String> vids = new LinkedList<>();
        String youtube = "https://www.youtube.com/embed/";
        for (Video video : movieDB.getVideos()) {
            switch (video.getSite()) {
                case "YouTube":
                    vids.add(youtube + video.getKey());
                    break;
                default:
                    break;
            }
        }
        youtubeLinks = vids;
    }

    @Override
    public boolean onWatchlist() {
        return loggedUser.getMovieWatchlist().getMovies().contains(movie);
    }

    @Override
    public boolean onFavoritelist() {
        return loggedUser.getMovieFavorite().getMovies().contains(movie);
    }

    @Override
    public void addToWatchList() {
        if (loggedUser.getMovieWatchlist().getMovies().add(new Movie(movieDB))) {
            userService.updateUser(loggedUser);
            addInfoMessage(null, "movie.watchlist.add", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public void addToFavoriteList() {
        if (loggedUser.getMovieFavorite().getMovies().add(new Movie(movieDB))) {
            userService.updateUser(loggedUser);
            addInfoMessage(null, "movie.favorite.add", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public void removeFromFavoriteList() {
        if (loggedUser.getMovieFavorite().getMovies().remove(new Movie(movieDB))) {
            userService.updateUser(loggedUser);
            addInfoMessage(null, "movie.favorite.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public void removeFromWatchList() {
        if (loggedUser.getMovieWatchlist().getMovies().remove(new Movie(movieDB))) {
            userService.updateUser(loggedUser);
            addInfoMessage(null, "movie.watchlist.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public void createNewCustomList() {
        if (loggedUser != null) {
            MovieList list = new MovieList(new LinkedList<>(), customListName, customListDesc, customListPublicity);
            if (loggedUser.getMovieCustomLists().add(list)) {
                loggedUser = userService.updateUser(loggedUser);
                addInfoMessage(null, "customList.add.successfull", null);
            } else {
                addInfoMessage(null, "customList.add.failed", null);
            }
            customListDesc = "";
            customListName = "";
            customListPublicity = false;
        } else {
            loggedUser = userService.findByUsername(loggedUser.getUsername());
        }
    }

    @Override
    public boolean onCustomlist(MovieList list) {
        return list.getMovies().contains(movie);
    }

    @Override
    public void addToCustomlist(String name) {
        for (MovieList list : loggedUser.getMovieCustomLists()) {
            if (list.getName().equals(name)) {
                if (list.addMovie(list, movie)) {
                    loggedUser = userService.updateUser(loggedUser);
                    addInfoMessage(null, "movie.customList.add", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
            }
        }
    }

    @Override
    public void removeFromCustomlist(String name) {
        for (MovieList list : loggedUser.getMovieCustomLists()) {
            if (list.getName().equals(name)) {
                if (list.removeMovie(list, movie)) {
                    loggedUser = userService.updateUser(loggedUser);
                    addInfoMessage(null, "movie.customList.remove", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }

            }
        }
    }

    @Override
    public boolean isCustomListPublicity() {
        return customListPublicity;
    }

    @Override
    public void setCustomListPublicity(boolean customListPublicity) {
        this.customListPublicity = customListPublicity;
    }

    //GETTER & SETTER: 
    /**
     *
     * @return
     */
    public List<String> getYoutubeLinks() {
        return youtubeLinks;
    }

    /**
     *
     * @return
     */
    public List<Movie> getSimilar() {
        return similar;
    }

    /**
     *
     * @param similar
     */
    public void setSimilar(List<Movie> similar) {
        this.similar = similar;
    }

    /**
     *
     * @param youtubeLinks
     */
    public void setYoutubeLinks(List<String> youtubeLinks) {
        this.youtubeLinks = youtubeLinks;
    }

    @Override
    public String getCustomListName() {
        return customListName;
    }

    @Override
    public void setCustomListName(String customListName) {
        this.customListName = customListName;
    }

    @Override
    public String getCustomListDesc() {
        return customListDesc;
    }

    @Override
    public void setCustomListDesc(String customListDesc) {
        this.customListDesc = customListDesc;
    }

    /**
     *
     * @return
     */
    public MovieDb getMovie() {
        return movieDB;
    }

    /**
     *
     * @param movie
     */
    public void setMovie(MovieDb movie) {
        this.movieDB = movie;
    }

    /**
     *
     * @return
     */
    public TagCloudModel getTagModel() {
        String l = "Kein Tag";
        if (tagModel != null) {
            l = tagModel.toString();
        }
        return tagModel;
    }

    /**
     *
     * @param tagModel
     */
    public void setTagModel(TagCloudModel tagModel) {
        this.tagModel = tagModel;
    }

    /**
     *
     * @return
     */
    public Credits getCredits() {
        return credits;
    }

    /**
     *
     * @param credits
     */
    public void setCredits(Credits credits) {
        this.credits = credits;
    }

}
