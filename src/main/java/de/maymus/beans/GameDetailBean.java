/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Game;
import de.maymus.models.GameList;
import de.maymus.models.User;
import de.maymus.models.api.GameDetail;
import de.maymus.models.api.ReleaseDate;
import de.maymus.services.GameService;
import de.maymus.services.UserService;
import de.maymus.util.Constants;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.tagcloud.TagCloudModel;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class GameDetailBean extends BaseBean implements Serializable, IDetailBean<GameList> {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient SessionBean sessionBean;

    private transient GameService gameService;
    private transient UserService userService;

    private int id;
    private Game gameForDb;
    private GameDetail game;
    private TagCloudModel tagModel;
    private User loggedUser;
    private List<String> youtubeLinks = new LinkedList<>();
    private List<Game> similar = new LinkedList<>();
    private String customListName;
    private String customListDesc;
    private boolean customListPublicity = false;
    private List<String> releases = new LinkedList<>();

    /**
     * Initialize the Bean
     */
    @PostConstruct
    public void initBean() {
        log.info("..... Starting <GameListBean> .....");
        userService = new UserService();
        gameService = new GameService();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            String ids = params.get("id");
            id = Integer.valueOf(ids);
        } catch (NumberFormatException e) {

        }

        if (sessionBean != null && sessionBean.getLoggedUser() != null) {
            this.loggedUser = sessionBean.getLoggedUser();
        }
    }

    /**
     * Load game data from api. Return path to error page when no game found.
     *
     * @return
     */
    public String requestGame() {
        if (id != 0) {
            game = gameService.getGameIgdbDetailById(id);
        }
        if (game != null) {
            gameForDb = gameService.createOrUpdate(new Game(game));
            generateReleaseDates();
            generateYoutubeLink();
            return "";

        } else {
            log.info("No Game found by ID: " + id);
            addWarnMessage(null, "error.again", null);
            return redirect404();
        }
    }

    private void generateReleaseDates() {
        List<Integer> lst = new LinkedList<>();
        if (game.getReleaseDates() != null) {
            game.getReleaseDates().forEach((t) -> {
                lst.add(t.getPlatform());
            });
            Map<Integer, String> platform = gameService.getGameIgdbReleaseDates(lst);
            game.getReleaseDates().forEach((t) -> {
                String reg = t.getRegion() != null ? " (" + getMsgFromBundle(Constants.gameRegion(t.getRegion())) + ")" : "";
                releases.add(t.getHuman() + reg + " -> " + platform.get(t.getPlatform()));
            });
        }

    }

    private void generateYoutubeLink() {
        if (game.getVideos() != null) {
            game.getVideos().forEach((x) -> {
                youtubeLinks.add(YOUTUBE + x.getVideoId());
            });
        }
    }

    @Override
    public boolean onFavoritelist() {
        return loggedUser.getGames_favorite().getGames().contains(gameForDb);
    }

    @Override
    public void addToFavoriteList() {
        if (loggedUser.getGames_favorite().getGames().add(gameForDb)) {
            userService.updateUser(loggedUser);
            addInfoMessage(null, "game.favorite.add", null);
        } else {
            addWarnMessage(null, "error.again", "CHANGE");
        }
    }

    @Override
    public void removeFromFavoriteList() {
        if (loggedUser.getGames_favorite().getGames().remove(gameForDb)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "game.favorite.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public boolean onWatchlist() {
        return loggedUser.getGames_watchlist().getGames().contains(gameForDb);
    }

    @Override
    public void addToWatchList() {
        if (loggedUser.getGames_watchlist().getGames().add(gameForDb)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "game.watchlist.add", null);
        } else {
            addWarnMessage(null, "error.again", "CHANGE");
        }
    }

    @Override
    public void removeFromWatchList() {
        if (loggedUser.getGames_watchlist().getGames().remove(gameForDb)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "game.watchlist.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public boolean onCustomlist(GameList list) {
        return list.getGames().contains(gameForDb);
    }

    @Override
    public void addToCustomlist(String name) {
        for (GameList list : loggedUser.getGames_customLists()) {
            if (list.getName().equals(name)) {
                if (list.addGame(list, gameForDb)) {
                    loggedUser = userService.updateUser(loggedUser);
                    sessionBean.setLoggedUser(loggedUser);
                    addInfoMessage(null, "game.customList.add", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
            }
        }
    }

    @Override
    public void removeFromCustomlist(String name) {
        for (GameList list : loggedUser.getGames_customLists()) {
            if (list.getName().equals(name)) {
                if (list.removeGame(list, gameForDb)) {
                    loggedUser = userService.updateUser(loggedUser);
                    sessionBean.setLoggedUser(loggedUser);
                    addInfoMessage(null, "game.customList.remove", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
            }
        }
    }

    @Override
    public void createNewCustomList() {
        GameList list = new GameList(customListName, customListDesc, customListPublicity, new LinkedList<>());
        if (loggedUser.getGames_customLists().add(list)) {
            loggedUser = userService.updateUser(loggedUser);
            addInfoMessage(null, "customList.add.successfull", null);
        } else {
            addInfoMessage(null, "customList.add.failed", null);
        }
        sessionBean.setLoggedUser(loggedUser);
        customListDesc = "";
        customListName = "";
    }

    /**
     * Return Icons for external Websites
     *
     * @param x
     * @return
     */
    public String shareIcon(int x) {
        return Constants.getGameLinkIcon(x);
    }

    /**
     * Return Name from external Websites
     *
     * @param x
     * @return
     */
    public String shareName(int x) {
        return Constants.getGameLinkString(x);
    }

    /**
     * Return Name of the game's category
     *
     * @param x
     * @return
     */
    public String gameCategory(int x) {
        String cat = Constants.getGameCategoryString(x);
        if (cat != null) {
            return getMsgFromBundle(cat);
        }
        return "-";
    }

    /**
     * Return Path to pegi image
     *
     * @param x
     * @return
     */
    public String pegiImage(int x) {
        return Constants.pegiLocation(x);
    }

    //
    //GETTER & SETTER
    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public GameDetail getGame() {
        return game;
    }

    /**
     *
     * @param game
     */
    public void setGame(GameDetail game) {
        this.game = game;
    }

    /**
     *
     * @return
     */
    public TagCloudModel getTagModel() {
        return tagModel;
    }

    /**
     *
     * @param tagModel
     */
    public void setTagModel(TagCloudModel tagModel) {
        this.tagModel = tagModel;
    }

    /**
     *
     * @return
     */
    public List<String> getYoutubeLinks() {
        return youtubeLinks;
    }

    /**
     *
     * @param youtubeLinks
     */
    public void setYoutubeLinks(List<String> youtubeLinks) {
        this.youtubeLinks = youtubeLinks;
    }

    /**
     *
     * @return
     */
    public List<Game> getSimilar() {
        return similar;
    }

    /**
     *
     * @param similar
     */
    public void setSimilar(List<Game> similar) {
        this.similar = similar;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCustomListName() {
        return customListName;
    }

    /**
     *
     * @param customListName
     */
    @Override
    public void setCustomListName(String customListName) {
        this.customListName = customListName;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCustomListDesc() {
        return customListDesc;
    }

    /**
     *
     * @param customListDesc
     */
    @Override
    public void setCustomListDesc(String customListDesc) {
        this.customListDesc = customListDesc;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isCustomListPublicity() {
        return customListPublicity;
    }

    /**
     *
     * @param customListPublicity
     */
    @Override
    public void setCustomListPublicity(boolean customListPublicity) {
        this.customListPublicity = customListPublicity;
    }

    /**
     *
     * @return
     */
    public List<String> getReleases() {
        return releases;
    }

    /**
     *
     * @param releases
     */
    public void setReleases(List<String> releases) {
        this.releases = releases;
    }

}
