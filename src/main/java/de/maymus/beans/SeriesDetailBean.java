/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Series;
import de.maymus.models.SeriesList;
import de.maymus.models.User;
import de.maymus.services.SeriesService;
import de.maymus.services.UserService;
import de.maymus.util.TimeHelper;
import info.movito.themoviedbapi.model.Credits;
import info.movito.themoviedbapi.model.Video;
import info.movito.themoviedbapi.model.tv.TvSeries;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.tagcloud.TagCloudModel;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class SeriesDetailBean extends BaseBean implements Serializable, IDetailBean<SeriesList> {

    private static final long serialVersionUID = 1L;

    @Inject
    private transient SessionBean sessionBean;

    private transient SeriesService seriesService;
    private transient UserService userService;

    private int id;
    private TvSeries seriesDb;
    private Series series;
    private TagCloudModel tagModel;
    private Credits credits;
    private int cast = 1;
    private int crew = 1;
    private User loggedUser;
    private List<String> youtubeLinks = new LinkedList<>();
    private List<Series> similar = new LinkedList<>();
    private String customListName;
    private String customListDesc;
    private boolean customListPublicity = false;

    /**
     * Initialize Bean
     */
    @PostConstruct
    public void initBean() {
        log.info("..... Starting <SeriesListBean> .....");
        userService = new UserService();
        seriesService = new SeriesService();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            String ids = params.get("id");
            id = Integer.valueOf(ids);
        } catch (NumberFormatException e) {

        }

        if (sessionBean != null && sessionBean.getLoggedUser() != null) {
            this.loggedUser = sessionBean.getLoggedUser();
        }
    }

    /**
     * Load Series. Return error path when series not found
     *
     * @return
     */
    public String requestSeries() {
        if (id != 0) {
            seriesDb = seriesService.getSeriesById(id);
            series = seriesService.createOrUpdate(new Series(seriesDb));
        }
        if (seriesDb != null) {
            credits = seriesDb.getCredits();
            generateYoutubeLink();

        } else {
            log.info("No Seires found by Id: " + id);
            addErrorMessage("KEine Seire. ÄNGDERN", null, null);
            return redirect404();
        }
        return "";
    }

    private void generateYoutubeLink() {
        List<String> vids = new LinkedList<>();
        String youtube = "https://www.youtube.com/embed/";
        for (Video video : seriesDb.getVideos()) {
            switch (video.getSite()) {
                case "YouTube":
                    vids.add(youtube + video.getKey());
                    break;
                default:
                    break;
            }
        }
        youtubeLinks = vids;

    }

    @Override
    public boolean onFavoritelist() {
        return loggedUser.getSeries_favorite().getSeries().contains(series);
    }

    @Override
    public void addToFavoriteList() {
        if (loggedUser.getSeries_favorite().getSeries().add(series)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "series.favorite.add", null);
        } else {
            addWarnMessage(null, "error.again", "CHANGE");
        }
    }

    @Override
    public void removeFromFavoriteList() {
        if (loggedUser.getSeries_favorite().getSeries().remove(series)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "series.favorite.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public boolean onWatchlist() {
        return loggedUser.getSeries_watchlist().getSeries().contains(series);
    }

    @Override
    public void addToWatchList() {
        if (loggedUser.getSeries_watchlist().getSeries().add(series)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "series.watchlist.add", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public void removeFromWatchList() {
        if (loggedUser.getSeries_watchlist().getSeries().remove(series)) {
            userService.updateUser(loggedUser);
            sessionBean.setLoggedUser(loggedUser);
            addInfoMessage(null, "series.watchlist.remove", null);
        } else {
            addWarnMessage(null, "error.again", null);
        }
    }

    @Override
    public boolean onCustomlist(SeriesList list) {
        return list.getSeries().contains(series);
    }

    @Override
    public void addToCustomlist(String name) {
        for (SeriesList list : loggedUser.getSeries_customLists()) {
            if (list.getName().equals(name)) {
                if (list.addSeries(list, series)) {
                    loggedUser = userService.updateUser(loggedUser);
                    sessionBean.setLoggedUser(loggedUser);
                    addInfoMessage(null, "series.customList.add", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
            }
        }
    }

    @Override
    public void removeFromCustomlist(String name) {
        for (SeriesList list : loggedUser.getSeries_customLists()) {
            if (list.getName().equals(name)) {
                if (list.removeSeries(list, series)) {
                    loggedUser = userService.updateUser(loggedUser);
                    sessionBean.setLoggedUser(loggedUser);
                    addInfoMessage(null, "series.customList.remove", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
            }
        }
    }

    /**
     *
     * @param path
     * @return
     */
    public String getImage(String path) {
        return posterImageMD(path);
    }

    @Override
    public void createNewCustomList() {
        SeriesList list = new SeriesList(new LinkedList<>(), customListName, customListDesc, customListPublicity);
        if (loggedUser.getSeries_customLists().add(list)) {
            loggedUser = userService.updateUser(loggedUser);
            addInfoMessage(null, "customList.add.successfull", null);
        } else {
            addInfoMessage(null, "customList.add.failed", null);
        }
        sessionBean.setLoggedUser(loggedUser);
        customListDesc = "";
        customListName = "";
    }

    /**
     * Convert and return Episode runtime
     *
     * @return
     */
    public String episodeRuntime() {
        String ret = "";
        for (int i = 0; i < seriesDb.getEpisodeRuntime().size(); i++) {
            ret = ret + Integer.toString(seriesDb.getEpisodeRuntime().get(i)) + " ,";
        }
        return ret.length() > 0 ? ret.substring(0, ret.lastIndexOf(",") - 1) : "-";
    }

    /**
     * Covnert and return First airing year
     *
     * @return
     */
    public String seriesFirstAiringYear() {
        return TimeHelper.inputStringToYear(seriesDb.getFirstAirDate());
    }

    /**
     * Convert and erturn first airing date
     *
     * @return
     */
    public String seriesFirstAiring() {
        return TimeHelper.convertUsToDe(seriesDb.getFirstAirDate());
    }

    /**
     * Convert and return last airing date
     *
     * @return
     */
    public String seriesLastAiring() {
        return TimeHelper.convertUsToDe(seriesDb.getFirstAirDate());
    }

    //
    //
    //
    //GETTER & SETTER
    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public TvSeries getSeriesDb() {
        return seriesDb;
    }

    /**
     *
     * @param seriesDb
     */
    public void setSeriesDb(TvSeries seriesDb) {
        this.seriesDb = seriesDb;
    }

    /**
     *
     * @return
     */
    public Series getSeries() {
        return series;
    }

    /**
     *
     * @param series
     */
    public void setSeries(Series series) {
        this.series = series;
    }

    /**
     *
     * @return
     */
    public TagCloudModel getTagModel() {
        return tagModel;
    }

    /**
     *
     * @param tagModel
     */
    public void setTagModel(TagCloudModel tagModel) {
        this.tagModel = tagModel;
    }

    /**
     *
     * @return
     */
    public Credits getCredits() {
        return credits;
    }

    /**
     *
     * @param credits
     */
    public void setCredits(Credits credits) {
        this.credits = credits;
    }

    /**
     *
     * @return
     */
    public int getCast() {
        return cast;
    }

    /**
     *
     * @param cast
     */
    public void setCast(int cast) {
        this.cast = cast;
    }

    /**
     *
     * @return
     */
    public int getCrew() {
        return crew;
    }

    /**
     *
     * @param crew
     */
    public void setCrew(int crew) {
        this.crew = crew;
    }

    /**
     *
     * @return
     */
    public User getLoggedUser() {
        return loggedUser;
    }

    /**
     *
     * @param loggedUser
     */
    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }

    /**
     *
     * @return
     */
    public List<String> getYoutubeLinks() {
        return youtubeLinks;
    }

    /**
     *
     * @param youtubeLinks
     */
    public void setYoutubeLinks(List<String> youtubeLinks) {
        this.youtubeLinks = youtubeLinks;
    }

    /**
     *
     * @return
     */
    public List<Series> getSimilar() {
        return similar;
    }

    /**
     *
     * @param similar
     */
    public void setSimilar(List<Series> similar) {
        this.similar = similar;
    }

    @Override
    public String getCustomListName() {
        return customListName;
    }

    @Override
    public void setCustomListName(String customListName) {
        this.customListName = customListName;
    }

    @Override
    public String getCustomListDesc() {
        return customListDesc;
    }

    @Override
    public void setCustomListDesc(String customListDesc) {
        this.customListDesc = customListDesc;
    }

    /**
     *
     * @return
     */
    public List<SeriesList> getCustomLists() {
        return sessionBean.getLoggedUser().getSeries_customLists();
    }

    @Override
    public boolean isCustomListPublicity() {
        return customListPublicity;
    }

    @Override
    public void setCustomListPublicity(boolean customListPublicity) {
        this.customListPublicity = customListPublicity;
    }

}
