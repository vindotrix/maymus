/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Game;
import de.maymus.models.GameList;
import de.maymus.models.User;
import de.maymus.services.GameListService;
import de.maymus.services.GameService;
import de.maymus.services.UserService;
import de.maymus.util.Constants;
import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Kevin
 */
@Named
@ViewScoped
public class GameListBean extends BaseBean implements Serializable, IListBean {

    private static final long serialVersionUID = 1L;

    private UserService userService;
    private GameService gameService;
    private GameListService gameListService;

    @Inject
    private transient SessionBean sessionBean;

    private GameList gameList;
    private String paramsType;
    private String paramsList;
    private String paramsUser;
    private User loggedUser = null;
    private String listname;
    private String editName;
    private String editDesc;
    private boolean editPublicity;
    private boolean userList = false;
    private boolean publicList = false;

    /**
     *
     */
    @PostConstruct
    public void initBean() {
        log.info("..... Starting <GameListBean> .....");
        userService = new UserService();
        gameService = new GameService();
        gameListService = new GameListService();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            paramsList = params.get("list");
            paramsType = params.get("type");
            paramsUser = params.get("user");
        } catch (Exception e) {
            System.out.println("");
        }

        isLogged();

    }

    private boolean isLogged() {
        if (sessionBean != null && sessionBean.isLoggedIn()) {
            loggedUser = sessionBean.getLoggedUser();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Request an GameList (Best rated, recently released, upcomming, watchlist,
     * favorite)
     *
     * @return
     */
    public String requestGameList() {
        if (paramsType != null && paramsType.length() > 0 && paramsList != null && paramsList.length() > 0) {
            switch (paramsType) {
                case Constants.LIST_TYPE_MAYMUS:
                    userList = false;
                    return requestMaymusList();
                case Constants.LIST_TYPE_USER:
                    if (isLogged()) {
                        userList = true;
                        return requestUserList();
                    } else {
                        return redirectToHomeNotLoggedIn();
                    }
            }
        }
        return redirect404();
    }

    private String requestMaymusList() {
        if (paramsList.equals(Constants.TOP_RATED) || paramsList.equals(Constants.UPCOMMING) || paramsList.equals(Constants.NEW_RELEASED)) {
            gameList = gameListService.findGameListByName(paramsList);
            listname = Constants.gameListName(paramsList);
            if (gameList != null) {
                return "";
            }
        }
        return redirect404();
    }

    private String requestUserList() {
        switch (paramsList) {
            case Constants.FAVORITE:
                gameList = loggedUser.getGames_favorite();
                break;
            case Constants.WATCH:
                gameList = loggedUser.getGames_watchlist();
                break;
        }
        if (gameList != null) {
            listname = Constants.userListName(paramsList);
            return "";
        }
        return redirect404();
    }

    /**
     * Request custom game list
     *
     * @return url to home if list not available
     */
    public String requestUserCustomList() {
        if (isLogged()) {
            for (GameList sl : loggedUser.getGames_customLists()) {
                if (paramsList.equals(sl.getName())) {
                    gameList = sl;
                    listname = sl.getName();
                    editName = listname;
                    editDesc = sl.getDesc();
                    editPublicity = sl.isShare();
                }
            }
            if (gameList != null) {
                return "";
            }
            return redirect404();
        } else if (paramsUser != null) {
            for (GameList list : userService.findByUsername(paramsUser).getGames_customLists()) {
                if (list.getName().equals(paramsList) && list.isShare()) {
                    gameList = list;
                    listname = paramsUser + "' Liste: " + list.getName();
                    publicList = true;
                    return "";
                }
            }
        }
        return redirectToHomeNotLoggedIn();
    }

    /**
     *
     * @return
     */
    public GameList getGameList() {
        return gameList;
    }

    /**
     *
     * @param gameList
     */
    public void setGameList(GameList gameList) {
        this.gameList = gameList;
    }

    @Override
    public String getListname() {
        return listname;
    }

    @Override
    public void setListname(String listname) {
        this.listname = listname;
    }

    @Override
    public String getEditName() {
        return editName;
    }

    @Override
    public void setEditName(String editName) {
        this.editName = editName;
    }

    @Override
    public String getEditDesc() {
        return editDesc;
    }

    @Override
    public void setEditDesc(String editDesc) {
        this.editDesc = editDesc;
    }

    @Override
    public boolean isEditPublicity() {
        return editPublicity;
    }

    @Override
    public void setEditPublicity(boolean editPublicity) {
        this.editPublicity = editPublicity;
    }

    /**
     *
     * @return
     */
    public String lastEdit() {
        return TimeHelper.prettyDateAndTime(gameList.getLast_edit());
    }

    /**
     *
     * @return
     */
    public boolean isUserList() {
        return userList;
    }

    /**
     *
     * @param userList
     */
    public void setUserList(boolean userList) {
        this.userList = userList;
    }

    @Override
    public String editList() {
        if (isLogged() && gameList != null) {
            gameList.setDesc(editDesc);
            gameList.setName(editName);
            gameList.setShare(editPublicity);
            loggedUser = userService.updateUser(loggedUser);
            listname = editName;

            addInfoMessage(null, "customList.edit.success", null);
            setMessageKeep();
            String x = "/maymus/pages/game/custom_list.xhtml?list=" + editName;
            return x;
        }
        addErrorMessage(null, "customList.edit.fail", null);
        return "/pages/game/custom_list.xhtml?faces-redirect=true";
    }

    @Override
    public String removeList() {
        if (isLogged() && gameList != null) {
            if (loggedUser.getGames_customLists().remove(gameList)) {
                loggedUser = userService.updateUser(loggedUser);
                sessionBean.setLoggedUser(loggedUser);
                addInfoMessage(null, "customList.remove.success", null);
                setMessageKeep();
                return "/pages/games/custom.xhtml?faces-redirect=true";
            }
        }
        addErrorMessage(null, "customList.remove.fail", null);
        setMessageKeep();
        return "/pages/game/custom.xhtml?faces-redirect=true";
    }

    @Override
    public void removeFromList(int id) {

        if (isLogged()) {
            for (Game tv : gameList.getGames()) {
                if (tv.getGame_id() == id) {
                    if (gameList.removeGame(gameList, tv)) {
                        loggedUser = userService.updateUser(loggedUser);
                        sessionBean.setLoggedUser(loggedUser);
                        addInfoMessage(null, "game.customList.remove", null);
                    } else {
                        addWarnMessage(null, "error.again", null);
                    }
                    break;
                }
            }
        }
    }

    @Override
    public String posterImageMD(String url) {
        return url.length() > 0 ? IGDB_POSTERPATH + "720p" + "/" + url + ".jpg" : POSTERPATH_DEFAULT;
    }

    @Override
    public String posterImageXL(String url) {
        return url.length() > 0 ? IGDB_POSTERPATH + "1080p" + "/" + url + ".jpg" : POSTERPATH_DEFAULT;
    }

}
