/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

/**
 *
 * @author Kevin
 */
public interface IListBean {

    /**
     * Variable for editing a list
     *
     * @return
     */
    String editList();

    /**
     * Variable for editing a list
     *
     * @return
     */
    String getEditDesc();

    /**
     * Variable for editing a list
     *
     * @return
     */
    String getEditName();

    /**
     * Variable for editing a list
     *
     * @return
     */
    String getListname();

    /**
     * Variable for editing a list
     *
     * @return
     */
    boolean isEditPublicity();

    /**
     * Removes a item from a List in the bean
     *
     * @param id
     */
    void removeFromList(int id);

    /**
     * Removes the viewed list
     *
     * @return
     */
    String removeList();

    /**
     * Variable for editing a list
     *
     * @param editDesc
     */
    void setEditDesc(String editDesc);

    /**
     * Variable for editing a list
     *
     * @param editName
     */
    void setEditName(String editName);

    /**
     * Variable for editing a list
     *
     * @param editPublicity
     */
    void setEditPublicity(boolean editPublicity);

    /**
     * Variable for editing a list
     *
     * @param listname
     */
    void setListname(String listname);

}
