/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.beans;

import de.maymus.models.Movie;
import de.maymus.models.MovieList;
import de.maymus.models.User;
import de.maymus.services.MovieService;
import de.maymus.services.MovieListService;
import de.maymus.services.UserService;
import de.maymus.util.Constants;
import de.maymus.util.TimeHelper;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author vindotrix
 */
@Named
@ViewScoped
public class MovieListBean extends BaseBean implements Serializable, IListBean {

    private static final long serialVersionUID = 1L;

    private final UserService userService;
    private final MovieService movieAS;
    private final MovieListService mas;

    @Inject
    private transient SessionBean sessionBean;

    private MovieList userMovieList;
    private MovieList tmdbList;
    private final String param;
    private final String paramsUser;
    private User loggedUser;
    private String listname = "";
    private String editName;
    private String editDesc;
    private boolean editPublicity;
    private boolean publicList = false;

    /**
     *
     */
    public MovieListBean() {
        log.info("..... Starting <MovieListBean> .....");
        movieAS = new MovieService();
        userService = new UserService();
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        param = params.get("list");
        paramsUser = params.get("user");
        mas = new MovieListService();
    }

    /**
     *
     */
    public void initUser() {
        if (sessionBean != null && sessionBean.getLoggedUser() != null) {
            this.loggedUser = sessionBean.getLoggedUser();
        }
    }

    /**
     *
     * @return
     */
    public String initialize() {
        initUser();
        if (loggedUser != null) {
            if (!validateUserListParam()) {
                log.info("Wrong List name: " + param);
                addErrorMessage(null, "error.404", null);
            }
        } else {
            log.info("No User logged -> no list available: ");
            addWarnMessage(null, "user.not.logged.in", null);
        }
        return userMovieList != null ? "" : "/home?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String initializeTmdb() {
        if (validateApiListParam()) {
            tmdbList = mas.findMovieListByName(param);
            listname = Constants.movieListName(param);
        } else {
            log.info("Wrong List name: " + param);
            addErrorMessage(null, "error.404", null);
        }
        return (tmdbList != null && tmdbList.getMovies() != null) ? "" : "/home?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String initializeCustom() {
        initUser();

        if (loggedUser != null) {
            if (validateUserCustomListParam()) {
            } else {
                log.info("Wrong List name: " + param);
                addErrorMessage(null, "error.404", null);
            }
        } else if (paramsUser != null) {
            for (MovieList list : userService.findByUsername(paramsUser).getMovieCustomLists()) {
                if (list.getName().equals(param) && list.isShare()) {
                    userMovieList = list;
                    listname = paramsUser + "' Liste: " + list.getName();
                    publicList = true;
                    return "";
                }
            }
        } else {
            log.info("No User logged -> no list available: ");
            addWarnMessage(null, "user.not.logged.in", null);
        }
        return userMovieList != null ? "" : "/home?faces-redirect=true";

    }

    /**
     *
     * @param date
     * @return
     */
    @Override
    public String converteTimeToDE(String date) {
        return super.converteTimeToDE(date); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param url
     * @return
     */
    @Override
    public String posterImageXL(String url) {
        return super.posterImageXL(url); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param url
     * @return
     */
    @Override
    public String posterImageMD(String url) {
        return super.posterImageMD(url); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean validateUserListParam() {
        listname = Constants.userListName(param);
        if (param != null) {
            switch (param) {
                case Constants.FAVORITE:
                    userMovieList = loggedUser.getMovieFavorite();
                    return true;
                case Constants.WATCH:
                    userMovieList = loggedUser.getMovieWatchlist();
                    return true;
            }
        }
        return false;
    }

    private boolean validateApiListParam() {
        if (param != null) {
            switch (param) {
                case Constants.TOP_RATED:
                    return true;
                case Constants.UPCOMMING:
                    return true;
                case Constants.NOW_IN_CINEMA:
                    return true;
            }
        }
        return false;
    }

    private boolean validateUserCustomListParam() {
        if (param != null) {
            for (MovieList list : loggedUser.getMovieCustomLists()) {
                if (list.getName().equals(param)) {
                    userMovieList = list;
                    editDesc = list.getDesc();
                    editName = list.getName();
                    editPublicity = list.isShare();
                    listname = list.getName();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void removeFromList(int id) {
        for (Movie mov : userMovieList.getMovies()) {
            if (mov.getMovie_id() == id) {
                if (userMovieList.getMovies().remove(mov)) {
                    loggedUser = userService.updateUser(loggedUser);
                    sessionBean.setLoggedUser(loggedUser);
                    addInfoMessage(null, "movie.customList.remove", null);
                } else {
                    addWarnMessage(null, "error.again", null);
                }
                break;
            }
        }
    }

    /**
     *
     * @return
     */
    public MovieList getUserMovieList() {
        return userMovieList;
    }

    /**
     *
     * @param userMovieList
     */
    public void setUserMovieList(MovieList userMovieList) {
        this.userMovieList = userMovieList;
    }

    /**
     *
     * @return
     */
    public MovieList getTmdbList() {
        return tmdbList;
    }

    /**
     *
     * @param tmdbList
     */
    public void setTmdbList(MovieList tmdbList) {
        this.tmdbList = tmdbList;
    }

    @Override
    public String getListname() {
        return listname;
    }

    @Override
    public void setListname(String listname) {
        this.listname = listname;
    }

    /**
     *
     * @return
     */
    public SessionBean getSessionBean() {
        return sessionBean;
    }

    /**
     *
     * @param sessionBean
     */
    public void setSessionBean(SessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }

    @Override
    public String getEditName() {
        return editName;
    }

    @Override
    public void setEditName(String editName) {
        this.editName = editName;
    }

    @Override
    public String getEditDesc() {
        return editDesc;
    }

    @Override
    public void setEditDesc(String editDesc) {
        this.editDesc = editDesc;
    }

    @Override
    public boolean isEditPublicity() {
        return editPublicity;
    }

    @Override
    public void setEditPublicity(boolean editPublicity) {
        this.editPublicity = editPublicity;
    }

    @Override
    public String editList() {
        if (loggedUser != null && userMovieList != null) {
            userMovieList.setDesc(editDesc);
            userMovieList.setName(editName);
            userMovieList.setShare(editPublicity);
            userMovieList.setLast_edit(TimeHelper.getTime());
            loggedUser = userService.updateUser(loggedUser);
            addInfoMessage(null, "customList.edit.success", null);
            setMessageKeep();
            String x = "/pages/movie/custom_list.xhtml?faces-redirect=true&list=" + editName;
            return x;
        }
        addErrorMessage(null, "customList.edit.fail", null);
        return "/pages/movie/custom_overview.xhtml?faces-redirect=true";
    }

    @Override
    public String removeList() {
        if (loggedUser != null && userMovieList != null) {
            if (loggedUser.getMovieCustomLists().remove(userMovieList)) {
                loggedUser = userService.updateUser(loggedUser);
                sessionBean.setLoggedUser(loggedUser);
                addInfoMessage(null, "customList.remove.success", null);
                setMessageKeep();
                return "/pages/movie/custom_overview.xhtml?faces-redirect=true";
            }
        }
        addErrorMessage(null, "customList.remove.fail", null);
        return "/pages/movie/custom_overview.xhtml?faces-redirect=true";
    }

    /**
     *
     * @return
     */
    public String lastEdit() {
        return TimeHelper.prettyDateAndTime(getUserMovieList().getLast_edit());
    }

}
