/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import de.maymus.models.api.GameDetail;
import de.maymus.models.api.Platform;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class IgdbService {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(IgdbService.class);

    private final String API_KEY = "92ac6c518a02439733a8fcde636dded9";
    private final String URI = "https://api-2445582011268.apicast.io/";

    /**
     *
     */
    public IgdbService() {

        Unirest.setObjectMapper(new ObjectMapper() {
            private final com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }

    private JsonNode getResponseJSON(String path) {
        Future<HttpResponse<JsonNode>> response = Unirest.get(URI + path)
                .header("user-key", API_KEY)
                .header("Accept", "application/json")
                .asJsonAsync();

        try {
            return response.get().getBody();
        } catch (InterruptedException | ExecutionException ex) {
            LOG.error("Exception while getting JSON: " + ex);
            return null;
        }
    }

    //Platform
    /**
     *
     * @param id
     * @return
     */
    protected Map<Integer, String> igdb_game_platfroms(List<Integer> id) {
        String ids = "";

        for (Integer i : id) {
            ids = ids + i.toString() + ",";
        }
        if (ids.length() > 0) {
            ids = ids.substring(0, ids.length() - 1);
        }
        String path = "platforms/" + ids + "/?fields=id,name";
        String debug = getResponseJSON(path).toString();
        return getPlatformResponse(path);
    }

    private Map<Integer, String> getPlatformResponse(String path) {
        try {
            Future<HttpResponse<Platform[]>> response = Unirest.get(URI + path)
                    .header("user-key", API_KEY)
                    .header("Accept", "application/json")
                    .asObjectAsync(Platform[].class);
            Platform[] arrays = response.get().getBody();
            Map<Integer, String> map = new HashMap<>();
            for (Platform array : arrays) {
                map.put(array.getId(), array.getName());
            }
            return map;
        } catch (InterruptedException | ExecutionException | RuntimeException ex) {
            LOG.error("Exception while getting JSON: " + ex);
            return null;
        }
    }

    /*
        GAME DETAIL
     */
    /**
     *
     * @param id
     * @return
     */
    protected GameDetail igdb_game_detail(int id) {
        String fields = "fields="
                //Complete
                + "id,name,url,summary,rating,rating_count,game,category,"
                + "time_to_beat,player_perspectives,publishers,game_engines,"
                + "first_release_date,status,videos,websites,dlcs,external,"
                + "franchise,genres,developers.name,game_modes,"
                //LIMITATION
                + "cover,pegi.rating,keywords.name,release_dates,"
                + "expansions";
        String expand = "expand="
                + "genres,franchise,publishers,developers,game_engines,game_modes,game,keywords,dlcs,expansions,player_perspectives";
        String path = "games" + "/" + String.valueOf(id) + "?" + fields + "&" + expand;
        String str = getResponseJSON(path).toString();
        List<GameDetail> games = getDetailResponse(path);
        return games.size() > 0 ? games.get(0) : null;
    }

    private List<GameDetail> getDetailResponse(String path) {
        try {
            Future<HttpResponse<GameDetail[]>> response = Unirest.get(URI + path)
                    .header("user-key", API_KEY)
                    .header("Accept", "application/json")
                    .asObjectAsync(GameDetail[].class);
            GameDetail[] arrays = response.get().getBody();
            return Arrays.asList(arrays);
        } catch (InterruptedException | ExecutionException | RuntimeException ex) {
            LOG.error("Exception while getting JSON: " + ex);
            return null;
        }
    }

    //LISTS
    private List<GameDetail> getMaymusLists(String path) {
        path = path.replaceAll(" ", "-");
        try {
            Future<HttpResponse<GameDetail[]>> response = Unirest.get(URI + path)
                    .header("user-key", API_KEY)
                    .header("Accept", "application/json")
                    .asObjectAsync(GameDetail[].class);
            GameDetail[] arrays = response.get().getBody();
            return Arrays.asList(arrays);
        } catch (InterruptedException | ExecutionException | RuntimeException ex) {
            LOG.error("Exception while getting JSON: " + ex);
            return null;
        }
    }

    /**
     *
     * @return
     */
    protected List<GameDetail> getUpcommingList() {
        String path = "/games/?fields=id,name,first_release_date,cover.cloudinary_id,summary&order=first_release_date:asc&limit=36&filter[first_release_date][gte]="
                + System.currentTimeMillis();
        return getMaymusLists(path);
    }

    /**
     *
     * @return
     */
    protected List<GameDetail> getNewList() {
        String path = "games/?limit=36&fields=id,name,first_release_date,cover.cloudinary_id&order=first_release_date:desc&filter[first_release_date][lte]="
                + System.currentTimeMillis();
        return getMaymusLists(path);
    }

    /**
     *
     * @return
     */
    protected List<GameDetail> getTopList() {
        String path = "/games/?limit=36&filter[rating_count][gte]=50&fields=id,name,first_release_date,cover.cloudinary_id,summary,rating&order=rating:desc";
        return getMaymusLists(path);
    }

    protected List<GameDetail> getSearchGame(String keyword) {
        String path = "/games/?search="
                + keyword
                + "&fields=id,name,cover,summary,total_rating,first_release_date&&limit=20";
        return getMaymusLists(path);
    }

}
