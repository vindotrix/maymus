/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.rest;

import info.movito.themoviedbapi.TmdbApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vindotrix
 */
public class TmdbService {

    static final Logger LOG = LoggerFactory.getLogger(TmdbService.class);

    /**
     *
     */
    public static TmdbService INSTANCE = null;

    private TmdbApi tmdb = null;

    private final String apiKey = "42f8fdaa30f1d7486383a8dd9b6ef9ac";

    /**
     *
     */
    protected TmdbService() {
        LOG.info("== Tmdb Constructor start == ");
        tmdb = new TmdbApi(apiKey);
    }

    /**
     *
     * @return
     */
    public static TmdbService getInstance() {
        if (INSTANCE == null) {
            LOG.info("No Tmdb connection open -> open new one!");
            INSTANCE = new TmdbService();
        }
        return INSTANCE;
    }

    /**
     *
     * @return
     */
    public TmdbApi getTmdb() {
        return tmdb;
    }

}
