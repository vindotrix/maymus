/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.handler;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import static java.lang.String.format;
import java.util.Arrays;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class ResourceHandler {
    
    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ResourceHandler.class);
    private static final ResourceHandler INSTANCE = new ResourceHandler();
    
    private MongoClient mongo = null;
    private Datastore dataStore = null;
    private Morphia morphia = null;
    private final String dbName = "maymus";
    
    private ResourceHandler() {
    }
    
    public MongoClient getMongo() throws RuntimeException {
        if (mongo == null) {
            LOG.debug("Starting Mongo");
            MongoClientOptions.Builder options = MongoClientOptions.builder()
                    .connectionsPerHost(4)
                    .maxConnectionIdleTime((60 * 1_000))
                    .maxConnectionLifeTime((120 * 1_000));

            //MongoClientURI uri = new MongoClientURI("mongodb://adminMaymus:maymusLive2017@ds145193.mlab.com:45193/maymus");
            try {
                MongoCredential credential = MongoCredential.createCredential("maymusLive", dbName, "maymusBachelor".toCharArray());
                
                ServerAddress server = new ServerAddress("localhost", 27017);
                LOG.info("About to connect to MongoDB @ " + server.toString());
                mongo = new MongoClient(server, Arrays.asList(credential));
            } catch (MongoException ex) {
                LOG.error("An error occoured when connecting to MongoDB", ex);
            } catch (Exception ex) {
                LOG.error("An error occoured when connecting to MongoDB", ex);
            }
        }
        
        return mongo;
    }
    
    public Morphia getMorphia() {
        if (morphia == null) {
            LOG.debug("Starting Morphia");
            morphia = new Morphia();
            morphia.mapPackage("de.maymus.models");
            morphia.getMapper().getOptions().setStoreEmpties(true);
        }
        
        return morphia;
    }
    
    public Datastore getDatastore(String db) {
        if (dataStore == null) {
            LOG.debug("Starting DataStore on DB: ", dbName);
            dataStore = getMorphia().createDatastore(getMongo(), dbName);
        }
        
        return dataStore;
    }
    
    public Datastore getDatastore() {
        if (dataStore == null) {
            String dbName = "maymus";
            LOG.debug("Starting DataStore on DB: ", dbName);
            dataStore = getMorphia().createDatastore(getMongo(), dbName);
            dataStore.ensureIndexes();
            
        }
        
        return dataStore;
    }
    
    public void init() {
        LOG.debug("Bootstraping");
        getMongo();
        getMorphia();
        //getDatastore();
    }
    
    public void close() {
        LOG.info("Closing MongoDB connection");
        if (mongo != null) {
            try {
                mongo.close();
                LOG.debug("Nulling the connection dependency objects");
                mongo = null;
                morphia = null;
                dataStore = null;
            } catch (Exception e) {
                LOG.error(format("An error occurred when closing the MongoDB connection\n%s", e.getMessage()));
            }
        } else {
            LOG.warn("mongo object was null, wouldn't close connection");
        }
    }
    
    public static ResourceHandler getInstance() {
        return INSTANCE;
    }
}
