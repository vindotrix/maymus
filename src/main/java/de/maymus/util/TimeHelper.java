/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Schedule;

/**
 *
 * @author Kevin
 */
public class TimeHelper {

    private static final String PRETTYPATTERN = "dd.MM.yyyy - HH:mm";

    public TimeHelper() {
    }

    public static LocalDateTime getTime() {
        LocalDateTime currentTime = LocalDateTime.now();
        return currentTime;
    }

    public static String prettyDateAndTime(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PRETTYPATTERN);
        return time.format(formatter);
    }

    public static String inputStringToYear(String input) {
        try {
            final String OLD_FORMAT = "yyyy-MM-dd";
            final String YEAR_FORMAT = "yyyy";

            SimpleDateFormat sdf;
            Date d;

            sdf = new SimpleDateFormat(OLD_FORMAT);
            d = sdf.parse(input);
            sdf.applyPattern(YEAR_FORMAT);

            return sdf.format(d);
        } catch (ParseException ex) {
            return "-";
        }
    }

    public static String convertUsToDe(String input) {
        try {
            final String OLD_FORMAT = "yyyy-MM-dd";
            final String YEAR_FORMAT = "dd.MM.yyyy";

            SimpleDateFormat sdf;
            Date d;

            sdf = new SimpleDateFormat(OLD_FORMAT);
            d = sdf.parse(input);
            sdf.applyPattern(YEAR_FORMAT);

            return sdf.format(d);
        } catch (ParseException ex) {
            return input;
        }
    }

    public static String convertUsGameToDe(String input) {
        try {
            final String OLD_FORMAT = "yyyy-MMM-dd";
            final String YEAR_FORMAT = "dd.MMM.yyyy";

            SimpleDateFormat sdf;
            Date d;

            sdf = new SimpleDateFormat(OLD_FORMAT);
            d = sdf.parse(input);
            sdf.applyPattern(YEAR_FORMAT);

            return sdf.format(d);
        } catch (ParseException ex) {
            return input;
        }
    }

    public static String getDateFromTimestamp(Long x) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            String dateFormatted = formatter.format(x);
            return dateFormatted;
        } catch (Exception e) {
            return "";
        }

    }

    public static String getTimeFromTimestamp(int x) {

        int hours = x / 3600;
        int minutes = (x % 3600) / 60;
        int seconds = x % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}
