/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.util;

import de.maymus.models.Game;
import de.maymus.models.GameList;
import de.maymus.models.Movie;
import de.maymus.models.MovieList;
import de.maymus.models.Series;
import de.maymus.models.SeriesList;
import de.maymus.models.User;
import de.maymus.services.CredentialService;
import de.maymus.services.GameListService;
import de.maymus.services.GameService;
import de.maymus.services.MovieListService;
import de.maymus.services.MovieService;
import de.maymus.services.SeriesListService;
import de.maymus.services.SeriesService;
import de.maymus.services.UserService;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author vindotrix
 */
@Startup
@Singleton
public class Scheduler {

    @PostConstruct
    public void init() {
        System.out.println("~~~~~~~~~~~~~~~~~~ START APPLIKATION ~~~~~~~~~~~~~~~~~~");
        UserService us = new UserService();
        User admin = us.findByUsername("Maymus");
        if (admin == null) {
            CredentialService cs = new CredentialService();
            us.createUser(new User("Maymus", "contact.maymus@gmail.com", cs.createUserPassword("superuser")));
        }
        updateMovieLists();
        updateSeriesList();
        updateGamesList();
    }

    @Schedule(hour = "02", minute = "59", info = "5MinScheduler", persistent = false)
    public void doSomething() {
        System.out.println("** UPDATING SYSTEM LISTS: " + TimeHelper.prettyDateAndTime(TimeHelper.getTime()));
        updateMovieLists();
        updateSeriesList();
        updateGamesList();
    }

    private void updateSeriesList() {
        SeriesListService service = new SeriesListService();
        SeriesService ss = new SeriesService();

        List<Series> list = service.updateNowPLayingSeries(1);
        list.addAll(service.updateNowPLayingSeries(2));

        list.forEach((t) -> {
            ss.createOrUpdate(t);
        });
        service.createOrUpdateList(new SeriesList(list.subList(0, 36), Constants.NOW_IN_TV, ""));

        list = service.updateTopSeries(1);
        list.addAll(service.updateTopSeries(2));

        list.forEach((t) -> {
            ss.createOrUpdate(t);
        });
        service.createOrUpdateList(new SeriesList(list.subList(0, 36), Constants.TOP_RATED, ""));

    }

    private void updateGamesList() {
        GameListService gls = new GameListService();
        GameService gs = new GameService();

        //TOP RATED
        List<Game> list = gls.updateTopLists();
        list.forEach((t) -> {
            gs.createOrUpdate(t);
        });
        gls.createOrUpdateList(new GameList(Constants.TOP_RATED, "", false, list));

        //RECENTLY RELEASED
        list = gls.recentlyReleasedList();
        list.forEach((t) -> {
            gs.createOrUpdate(t);
        });
        gls.createOrUpdateList(new GameList(Constants.NEW_RELEASED, "", false, list));

        //UPCOMMING
        list = gls.updateUpcomming();
        list.forEach((t) -> {
            gs.createOrUpdate(t);
        });
        gls.createOrUpdateList(new GameList(Constants.UPCOMMING, "", false, list));

    }

    private void updateMovieLists() {
        System.out.println("** asdfasdf");
        MovieListService mlas = new MovieListService();
        MovieService movieService = new MovieService();

        List<Movie> list = new LinkedList<>();

        /**
         * GET AND STORE TOP RATED LIST
         */
        list = mlas.updateTopRated(1);
        list.addAll(mlas.updateTopRated(2));
        list.forEach((t) -> {
            movieService.createOrUpdate(t);
        });
        mlas.createOrUpdateList(new MovieList(list.subList(0, 36), Constants.TOP_RATED, "movie.list.tmdb.top", false));

        /**
         * GET AND STORE NOW IN CINEMA LIST
         */
        list = mlas.updateNowCinema(1);
        list.addAll(mlas.updateNowCinema(2));
        list.forEach((t) -> {
            movieService.createOrUpdate(t);
        });
        mlas.createOrUpdateList(new MovieList(list.subList(0, 36), Constants.NOW_IN_CINEMA, "movie.list.tmdb.recently", false));

        /**
         * GET AND STORE UPCOMMING LIST
         */
        list = mlas.updateUpcomming(1);
        list.addAll(mlas.updateUpcomming(2));
        list.forEach((t) -> {
            movieService.createOrUpdate(t);
        });
        mlas.createOrUpdateList(new MovieList(list.subList(0, 36), Constants.UPCOMMING, "movie.list.tmdb.upcomming", false));

    }

}
