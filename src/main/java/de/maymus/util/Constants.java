/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.util;

/**
 *
 * @author Kevin
 */
public class Constants {

    //MAYMUS
    public static final String TOP_RATED = "top-rated";
    public static final String UPCOMMING = "upcomming";
    public static final String NOW_IN_CINEMA = "now-cinema";
    public static final String NOW_IN_TV = "now-tv";
    public static final String NEW_RELEASED = "new-released";

    //USER
    public static final String FAVORITE = "favorite";
    public static final String WATCH = "watchlist";

    //TYPE
    public static final String LIST_TYPE_MAYMUS = "maymus";
    public static final String LIST_TYPE_USER = "user";
    public static final String LIST_TYPE_USER_CUSTOM = "user_custom";

    //MSG LISTS
    public static final String MSG_FAVORITE = "list.favorite";
    public static final String MSG_WATCHLIST = "list.watchlist";
    public static final String MSG_OWN = "list.custom";

    public static final String MSG_M_UPC = "movie.list.tmdb.upcomming";
    public static final String MSG_M_TOP = "movie.list.tmdb.top";
    public static final String MSG_M_NOW = "movie.list.tmdb.recently";

    public static final String MSG_S_NOW = "series.list.tmdb.now";
    public static final String MSG_S_TOP = "series.list.tmdb.top";

    public static final String MSG_G_UPC = "game.list.tmdb.upc";
    public static final String MSG_G_NOW = "game.list.tmdb.now";
    public static final String MSG_G_TOP = "game.list.tmdb.top";

    public static String userListName(String name) {
        switch (name) {
            case FAVORITE:
                return MSG_FAVORITE;
            case WATCH:
                return MSG_WATCHLIST;
            case TOP_RATED:
            default:
                return "";
        }
    }

    public static String seriesListName(String name) {
        switch (name) {
            case TOP_RATED:
                return MSG_S_TOP;
            case NOW_IN_TV:
                return MSG_S_NOW;
            default:
                return "-";
        }
    }

    public static String movieListName(String name) {
        switch (name) {
            case TOP_RATED:
                return MSG_M_TOP;
            case NOW_IN_CINEMA:
                return MSG_M_NOW;
            case UPCOMMING:
                return MSG_M_UPC;
            default:
                return "-";
        }
    }

    public static String gameListName(String name) {
        switch (name) {
            case TOP_RATED:
                return MSG_G_TOP;
            case NEW_RELEASED:
                return MSG_G_NOW;
            case UPCOMMING:
                return MSG_G_UPC;
            default:
                return "-";
        }
    }

    public static String getGameLinkIcon(int input) {
        switch (input) {
            case 1://Official
                return "external-link";
            case 2: //wikia
                return "file-text-o";
            case 3: //wikipedia
                return "wikipedia-w";
            case 4: //Facebook
                return "facebook";
            case 5: //twitter
                return "twitter";
            case 6: //Twitch
                return "twitch";
            case 8: //Insta
                return "instagram";
            case 9: //Youtube
                return "youtube";
            case 10: //Apple
                return "apple";
            case 11: //Apple
                return "apple";
            case 12: //Android
                return "android";
            case 13: //steam
                return "steam";
            default:
                return "share";
        }
    }

    public static String getGameLinkString(int input) {
        switch (input) {
            case 1://Official
                return "Homepage";
            case 2: //wikia
                return "Wikia";
            case 3: //wikipedia
                return "Wikipedia";
            case 4: //Facebook
                return "Facebook";
            case 5: //twitter
                return "Twitter";
            case 6: //Twitch
                return "Twitch";
            case 8: //Insta
                return "Instagram";
            case 9: //Youtube
                return "Youtube";
            case 10: //Apple
                return "iOS";
            case 11: //Apple
                return "iOS";
            case 12: //Android
                return "Android";
            case 13: //steam
                return "Steam";
            default:
                return "?";
        }
    }

    public static String getGameCategoryString(int x) {
        switch (x) {
            case 0: //
                return "game.category.main";
            case 1: //
                return "game.category.dlc";
            case 2: //
                return "game.category.expansion";
            case 3: //
                return "game.category.bundle";
            case 4: // 
                return "game.category.expansion.standalone";
            default: //
                return null;
        }
    }

    public static String pegiLocation(int x) {
        switch (x) {
            case 1: //
                return "/resources/images/pegi_3.png";
            case 2: //
                return "/resources/images/pegi_7.png";
            case 3: //
                return "/resources/images/pegi_12.png";
            case 4: //
                return "/resources/images/pegi_16.png";
            case 5: //
                return "/resources/images/pegi_18.png";
            default: //
                return "-";
        }
    }

    public static String gameRegion(int x) {
        switch (x) {
            case 1: //
                return "region.eu";
            case 2: //
                return "region.na";
            case 3: //
                return "region.au";
            case 4: //
                return "region.nz";
            case 5: //
                return "region.jp";
            case 6: //
                return "region.ch";
            case 7: //
                return "region.as";
            case 8: //
                return "region.ww";
            default: //
                return "-";
        }
    }

}
