/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.validation;

import de.maymus.services.UserService;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@RequestScoped
@FacesValidator("mailSignUpValidation")
public class MailSignUpValidator extends BaseValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        LOG.info("Validate Email.");
        if (value.toString().length() == 0) {
            LOG.info("Email is not valid: no Email entered!");
            throw new ValidatorException(getMsg("login.register.mail.empty"));
        }

        String pattern = "[a-zA-Z0-9._%+-äÄüÜöÖ]+@[a-zA-Z0-9.-äÄüÜöÖ]+\\.[a-zA-Z]{2,4}";
        if (!value.toString().matches(pattern)) {
            LOG.info("Email is not valid: no valid pattern!  " + value.toString());
            throw new ValidatorException(getMsg("login.register.mail.pattern"));
        }

        UserService us = new UserService();
        if (us.findUserByEMail(value.toString()) != null) {
            throw new ValidatorException(getMsg("login.register.mail.assign"));
        }

        LOG.info("Email is valid!");
    }

}
