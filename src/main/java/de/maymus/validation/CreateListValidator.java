/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.validation;

import de.maymus.beans.SessionBean;
import de.maymus.models.MovieList;
import de.maymus.models.User;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

/**
 *
 * @author Kevin
 */
@RequestScoped
@FacesValidator("createListValidation")
public class CreateListValidator extends BaseValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        LOG.info("VALIDATION");

        Object obj = component.getAttributes().get("loggedUser");
        Object typ = component.getAttributes().get("type");

        if (value.toString().length() == 0) {
            LOG.info("EMPTY");
            throw new ValidatorException(getMsg("customList.empty.name"));
        }
        User user;
        String type;
        try {
            user = (User) obj;
            type = (String) typ;
        } catch (Exception e) {
            throw new ValidatorException(getMsg("customList.add.failed"));
        }

        if (user != null) {
            switch (type) {
                case "movie":
                    user.getMovieCustomLists().stream().filter((list) -> (list.getName().equals(value.toString()))).forEachOrdered((_item) -> {
                        throw new ValidatorException(getMsg("customList.error.name"));
                    });
                    break;
                case "series":
                    user.getSeries_customLists().stream().filter((list) -> (list.getName().equals(value.toString()))).forEach((item) -> {
                        throw new ValidatorException(getMsg("customList.error.name"));
                    });
                    break;
                case "games":
                    user.getGames_customLists().stream().filter((list) -> (list.getName().equals(value.toString()))).forEach((item) -> {
                        throw new ValidatorException(getMsg("customList.error.name"));
                    });
                    break;
                default:
                    break;
            }
        }

    }
}
