/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.validation;

import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class BaseValidator {

    protected final transient org.slf4j.Logger LOG = LoggerFactory.getLogger(this.getClass());

    protected FacesMessage getMsg(String key) {
        FacesMessage facesMessage;
        String summary;
        final FacesContext facesContext = FacesContext.getCurrentInstance();

        final ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, "msg");
        summary = bundle.getString(key);

        return new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, null);
    }
}
