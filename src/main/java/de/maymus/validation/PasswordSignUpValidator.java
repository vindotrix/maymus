/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.validation;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
@RequestScoped
@FacesValidator("passwordSignUpValidation")
public class PasswordSignUpValidator extends BaseValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        LOG.info("Validate Password!");
        if (value.toString().length() < 6) {
            LOG.info("Password invalid. To short!");
            throw new ValidatorException(getMsg("login.register.password.length"));
        }

        String pattern = "((?=.*[a-zßöäü])(?=.*[A-ZÖÄÜ]).{4,16})";
        if (!value.toString().matches(pattern)) {
            LOG.info("Password invalid. No valid pattern! - " + value.toString());

            throw new ValidatorException(getMsg("login.register.password.pattern"));
        }

    }

}
