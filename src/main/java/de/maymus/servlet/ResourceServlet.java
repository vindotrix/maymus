/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.servlet;

import de.maymus.handler.ResourceHandler;
import java.io.Serializable;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 */
public class ResourceServlet implements ServletContextListener {

    static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ResourceServlet.class
    );

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("===== Mongo Servlet started ====");
        ResourceHandler conn = ResourceHandler.getInstance();
        conn.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOG.info("===== Mongo Servlet stopped ====");
        ResourceHandler conn = ResourceHandler.getInstance();
        conn.close();
    }

}
