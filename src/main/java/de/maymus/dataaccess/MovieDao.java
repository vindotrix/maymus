/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.models.Movie;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class MovieDao extends BaseDao<Movie> {

    public MovieDao() {
    }

    public MovieDao(Datastore ds_test) {
        ds = ds_test;
    }

    public Movie createOrUpdate(Movie movie) {
        LOG.info("Create or update movie: " + movie.getName());
        Query<Movie> query = ds.createQuery(Movie.class).field("movie_id").equal(movie.getMovie_id());
        UpdateOperations<Movie> ops = ds.createUpdateOperations(Movie.class)
                .set("name", (movie.getName() != null ? movie.getName() : ""))
                .set("poster", (movie.getPoster() != null ? movie.getPoster() : ""))
                .set("story", (movie.getStory() != null ? movie.getStory() : ""))
                .set("rating", movie.getRating())
                .set("releaseDate", (movie.getReleaseDate() != null ? movie.getReleaseDate() : 0))
                .set("runtime", (movie.getRuntime()));
        return createOrUpdate(query, ops);
    }

    public Movie findByMorphia_ID(Object id) {
        LOG.info("Find Movie by Morphia_id: " + id);
        Query<Movie> query = ds.createQuery(Movie.class).field("_id").equal(id);
        return find(query);
    }

    public Movie findById(int id) {
        LOG.info("Find Movie by id: " + id);
        Query<Movie> query = ds.createQuery(Movie.class).field("movie_id").equal(id);
        return find(query);
    }

    public boolean remove(Movie movie) {
        LOG.info("Remove movie by id: " + movie.getName());
        Query<Movie> query = ds.createQuery(Movie.class).field("movie_id").equal(movie.getMovie_id());
        return findAndRemove(query);
    }

    public List<Movie> getAll() {
        Query<Movie> query = ds.find(Movie.class);
        return getAllAsList(query);
    }

}
