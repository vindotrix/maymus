/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.handler.ResourceHandler;
import com.mongodb.DuplicateKeyException;
import static de.maymus.dataaccess.GameListDao.LOG;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.FindAndModifyOptions;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kevin
 * @param <T>
 */
public class BaseDao<T> {

    static final Logger LOG = LoggerFactory.getLogger(Object.class);

    Datastore ds;

    /**
     *
     */
    public BaseDao() {
        ds = ResourceHandler.getInstance().getDatastore();
    }

    /**
     * Save Object in Database
     *
     * @param object
     * @return the object whenn successfully save. null else
     */
    public T create(T object) {
        try {
            Key key = ds.save(object);
            return key.getId() != null ? object : null;
        } catch (DuplicateKeyException e) {
            LOG.error("Duplicate Key Exception, Object already exists: " + object.toString());
            return null;
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
            return null;
        }
    }

    /**
     * Update object
     *
     * @param query
     * @param operation
     * @return the given object when successfully updated, null else
     */
    public T update(Query<T> query, UpdateOperations<T> operation) {
        try {
            return ds.findAndModify(query, operation);
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
            return null;
        }
    }

    /**
     * Create or Update an object
     *
     * @param query
     * @param update
     * @return the object when successfully saved/udpated or null when failed
     */
    public T createOrUpdate(Query<T> query, UpdateOperations<T> update) {
        try {
            FindAndModifyOptions famo = new FindAndModifyOptions();
            famo.upsert(true).returnNew(true);
            return ds.findAndModify(query, update, famo);
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
        }
        return null;
    }

    /**
     * find object
     *
     * @param query
     * @return return object whenn found, null else
     */
    public T find(Query<T> query) {
        try {
            return query.get();
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
            return null;
        }
    }

    /**
     * getall from given type
     *
     * @param query
     * @return return all objects from given class
     */
    public List<T> getAllAsList(Query<T> query) {
        try {
            return query.asList();
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
            return null;
        }
    }

    /**
     * find and remove a object
     *
     * @param query
     * @return return true when removed. false else
     */
    protected boolean findAndRemove(Query<T> query) {
        try {
            return ds.findAndDelete(query) != null;
        } catch (Exception e) {
            LOG.error("Unknown exception: " + e);
            return false;
        }
    }

}
