/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.models.User;
import de.maymus.util.TimeHelper;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author vindotrix
 */
public class UserDao extends BaseDao<User> {

    public UserDao() {
    }

    public UserDao(Datastore ds_test) {
        ds = ds_test;
    }

    public User createUser(User user) {
        LOG.info("Create new User: " + user.toShortString());
        return create(user);
    }

    public User updateUser(User user) {
        LOG.info("Update User: " + user.getUsername());
        Query<User> query = ds.createQuery(User.class).field("username").equal(user.getUsername());
        UpdateOperations<User> ops = ds.createUpdateOperations(User.class)
                .set("email", user.getEmail())
                .set("password", user.getPassword())
                .set("last_online", TimeHelper.getTime())
                .set("language", user.getLanguage())
                .set("movie_favorite", user.getMovieFavorite())
                .set("movie_watchlist", user.getMovieWatchlist())
                .set("movie_customLists", user.getMovieCustomLists())
                .set("series_favorite", user.getSeries_favorite())
                .set("series_watchlist", user.getSeries_watchlist())
                .set("series_customLists", user.getSeries_customLists())
                .set("games_favorite", user.getGames_favorite())
                .set("games_watchlist", user.getGames_watchlist())
                .set("games_customLists", user.getGames_customLists());
        return update(query, ops);
    }

    public boolean remove(User user) {
        LOG.info("Delete User: " + user.toShortString());
        Query<User> query = ds.createQuery(User.class).field("username").equal(user.getUsername());
        return findAndRemove(query);

    }

    public User findUserByMail(String mail) {
        LOG.info("FindUserByMail: " + mail);
        Query<User> query = ds.createQuery(User.class).field("email").equal(mail);
        return find(query);
    }

    public User findUserByUsername(String username) {
        LOG.info("FindUserByUsername: " + username);
        Query<User> query = ds.createQuery(User.class).field("username").equal(username);
        return find(query);
    }

    public User findUserById(Object id) {
        LOG.info("FindUserById: " + id);
        Query<User> query = ds.createQuery(User.class).field("_id").equal(id);
        return find(query);
    }

    public List<User> getAllUser() {
        LOG.info("Get all User");
        Query<User> query = ds.find(User.class);
        return getAllAsList(query);
    }
}
