/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.handler.ResourceHandler;
import de.maymus.models.SeriesList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class SeriesListDao extends BaseDao<SeriesList> {

    /**
     *
     * @param ds
     */
    public SeriesListDao(Datastore ds) {
    }

    /**
     *
     */
    public SeriesListDao() {
        ds = ResourceHandler.getInstance().getDatastore();
    }

    /**
     *
     * @param list
     * @return
     */
    public SeriesList createOrUpdateList(SeriesList list) {
        try {
            LOG.info("CreateOrUpdateList: " + list.getName());
            Query<SeriesList> query = ds.createQuery(SeriesList.class).field("_id").equal(list.getName());
            UpdateOperations<SeriesList> ops = ds.createUpdateOperations(SeriesList.class)
                    .set("series", list.getSeries())
                    .set("last_edit", list.getLast_edit())
                    .set("_id", list.getName())
                    .set("share", list.isShare());
            return createOrUpdate(query, ops);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    /**
     *
     * @param name
     * @return
     */
    public SeriesList findMovieListByName(String name) {
        LOG.info("Find MovieList by Name: " + name);
        Query<SeriesList> query = ds.createQuery(SeriesList.class).field("_id").equal(name);
        return find(query);
    }

    /**
     *
     * @param ml
     * @return
     */
    public boolean remove(SeriesList ml) {
        LOG.info("Remove MovieList: " + ml);
        Query<SeriesList> query = ds.createQuery(SeriesList.class).field("list_name").equal(ml.getName());
        return findAndRemove(query);
    }

    /**
     *
     * @return
     */
    public List<SeriesList> getAll() {
        LOG.info("Get all SeriesLists!");
        Query<SeriesList> query = ds.find(SeriesList.class);
        return getAllAsList(query);
    }
}
