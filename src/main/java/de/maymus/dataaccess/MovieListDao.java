/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.models.MovieList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class MovieListDao extends BaseDao<MovieList> {
    
    public MovieListDao() {
    }
    
    public MovieListDao(Datastore ds_test) {
        ds = ds_test;
    }
    
    public MovieList createOrUpdateList(MovieList list) {
        LOG.info("CreateOrUpdateList: " + list.getName());
        try {
            Query<MovieList> query = ds.createQuery(MovieList.class).field("_id").equal(list.getName());
            UpdateOperations<MovieList> ops = ds.createUpdateOperations(MovieList.class)
                    .set("_id", list.getName())
                    .set("movies", list.getMovies())
                    .set("desc", list.getDesc())
                    .set("last_edit", list.getLast_edit())
                    .set("share", list.isShare());
            return createOrUpdate(query, ops);
        } catch (Exception e) {
            LOG.error("Exception: " + e);
            return null;
        }
    }
    
    public MovieList findMovieListByName(String ident) {
        LOG.info("Find MovieList by Name: " + ident);
        Query<MovieList> query = ds.createQuery(MovieList.class).field("_id").equal(ident);
        return find(query);
    }
    
    public boolean remove(MovieList ml) {
        LOG.info("Remove MovieList: " + ml);
        Query<MovieList> query = ds.createQuery(MovieList.class).field("list_name").equal(ml.getName());
        return findAndRemove(query);
    }
    
    public List<MovieList> getAll() {
        LOG.info("Get all MovieLists!");
        Query<MovieList> query = ds.find(MovieList.class);
        return getAllAsList(query);
    }
    
}
