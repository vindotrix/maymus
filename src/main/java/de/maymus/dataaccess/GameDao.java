/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.models.Game;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class GameDao extends BaseDao<Game> {

    public GameDao() {
    }

    public GameDao(Datastore ds_test) {
        ds = ds_test;
    }

    public Game createOrUpdate(Game game) {
        LOG.info("Create or update Game: " + game.getName());
        Query<Game> query = ds.createQuery(Game.class).field("_id").equal(game.getGame_id());
        UpdateOperations<Game> ops = ds.createUpdateOperations(Game.class)
                .set("name", (game.getName() != null ? game.getName() : ""))
                .set("poster", (game.getPoster() != null ? game.getPoster() : ""))
                .set("story", (game.getStory() != null ? game.getStory() : ""))
                .set("rating", game.getRating())
                .set("releaseDate", (game.getReleaseDate() != null ? game.getReleaseDate() : 0))
                .set("timestamp", (game.getTimestamp()));

        return createOrUpdate(query, ops);
    }

    public Game findById(int id) {
        LOG.info("Find Game by id: " + id);
        Query<Game> query = ds.createQuery(Game.class).field("_id").equal(id);
        return find(query);
    }

    public boolean remove(Game game) {
        LOG.info("Remove game by id: " + game.getName());
        Query<Game> query = ds.createQuery(Game.class).field("_id").equal(game.getGame_id());
        return findAndRemove(query);
    }

    public List<Game> getAll() {
        LOG.info("Get all games");
        Query<Game> query = ds.find(Game.class);
        return getAllAsList(query);
    }

}
