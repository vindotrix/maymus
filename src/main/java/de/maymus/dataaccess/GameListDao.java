/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import de.maymus.models.GameList;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class GameListDao extends BaseDao<GameList> {

    public GameListDao() {
    }

    public GameListDao(Datastore tds) {
        ds = tds;
    }

    public GameList createOrUpdateList(GameList list) {
        LOG.info("Createor Update Gamelist: " + list.getName());
        try {
            Query<GameList> query = ds.createQuery(GameList.class).field("_id").equal(list.getName());
            UpdateOperations<GameList> ops = ds.createUpdateOperations(GameList.class)
                    .set("games", list.getGames())
                    .set("last_edit", list.getLast_edit())
                    .set("_id", list.getName())
                    .set("share", list.isShare());
            return createOrUpdate(query, ops);
        } catch (Exception e) {
            LOG.error("Exception: " + e);
        }
        return null;
    }

    public GameList findGameListByName(String name) {
        LOG.info("Find GameList by Name: " + name);
        Query<GameList> query = ds.createQuery(GameList.class).field("_id").equal(name);
        return find(query);
    }

    public List<GameList> getAll() {
        LOG.info("Get all GameList!");
        Query<GameList> all = ds.find(GameList.class);
        return getAllAsList(all);
    }

    public boolean remove(GameList list) {
        LOG.info("Remove MovieList: " + list);
        Query<GameList> query = ds.createQuery(GameList.class).field("_id").equal(list.getName());
        return findAndRemove(query);
    }
}
