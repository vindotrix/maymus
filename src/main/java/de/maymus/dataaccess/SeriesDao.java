/* 
 * Copyright 2017 brach.kevin@stud.htwk-leipzig.de.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.maymus.dataaccess;

import static de.maymus.dataaccess.BaseDao.LOG;
import de.maymus.models.Series;
import de.maymus.util.TimeHelper;
import java.util.List;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

/**
 *
 * @author Kevin
 */
public class SeriesDao extends BaseDao<Series> {

    /**
     * Constructor
     */
    public SeriesDao() {
    }

    /**
     * Test constructor
     *
     * @param ds_test
     */
    public SeriesDao(Datastore ds_test) {
        ds = ds_test;
    }

    /**
     * CreateOrUdpate object
     *
     * @param series object to update
     * @return object or null
     */
    public Series createOrUpdate(Series series) {
        LOG.info("CreateOrUpdate seires: " + series.getName());
        Query<Series> query = ds.createQuery(Series.class).field("series_id").equal(series.getSeries_id());
        UpdateOperations<Series> ops = ds.createUpdateOperations(Series.class)
                .set("name", (series.getName() != null ? series.getName() : ""))
                .set("poster", (series.getPoster() != null ? series.getPoster() : ""))
                .set("story", (series.getStory() != null ? series.getStory() : ""))
                .set("rating", series.getRating())
                .set("releaseDate", (series.getReleaseDate() != null ? series.getReleaseDate() : ""))
                .set("status", (series.getStatus() != null ? series.getStatus() : ""))
                .set("timestamp", TimeHelper.getTime())
                .set("seasons", series.getSeasons())
                .set("episodes", series.getEpisodes());
        return createOrUpdate(query, ops);
    }

    /**
     * Find Series by ID
     *
     * @param id
     * @return return Series or null
     */
    public Series findById(int id) {
        LOG.info("Find Series by id: " + id);
        Query<Series> query = ds.createQuery(Series.class).field("series_id").equal(id);
        return find(query);
    }

    /**
     * Remove series
     *
     * @param series
     * @return true when successfull, false then
     */
    public boolean remove(Series series) {
        LOG.info("Remove Series: " + series.getSeries_id());
        Query<Series> query = ds.createQuery(Series.class).field("series_id").equal(series.getSeries_id());
        return findAndRemove(query);
    }

    /**
     * Get all series
     *
     * @return all series
     */
    public List<Series> getAll() {
        LOG.info("Get all series");
        Query<Series> query = ds.find(Series.class);
        return getAllAsList(query);
    }
}
