/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var sub_movie = "movie_subnav";
var sub_tv = "tv_subnav";
var sub_game = "games_subnav";
var nav_movie = "nav_movie";
var nav_tv = "nav_tv";
var nav_game = "nav_game";
var navbar = "header";
var subheader_background = '#40617d';


function calcHideNavbar() {
    hideAll();

}

function test(){
    alert('ALERT');
}
function toggle(id) {
    var x = document.getElementById(id);
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}



function hideAll(){
    document.getElementById(sub_movie).style.display='none';
    document.getElementById(sub_tv).style.display='none';
    document.getElementById(sub_game).style.display='none';
    document.getElementById(nav_movie).style.backgroundColor = document.getElementById(navbar).style.backgroundColor;
    document.getElementById(nav_tv).style.backgroundColor = document.getElementById(navbar).style.backgroundColor;
    document.getElementById(nav_game).style.backgroundColor = document.getElementById(navbar).style.backgroundColor;
    
    document.body.style.paddingTop = 65 + 'px';
    
}

function openSubheader(headerItem, subheader){
    hideAll();
    var shead = document.getElementById(subheader);
    var head = document.getElementById(headerItem);
    head.style.backgroundColor = subheader_background;
    if (shead.style.display === 'none') {
        shead.style.display = 'block';
        var height = 65 + shead.clientHeight;
        document.body.style.paddingTop = height + 'px';
    } 
}